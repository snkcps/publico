angular.module("ClientesGeovaneApp", ["snk"])
	.controller("ClientesGeovaneController",
		["$scope", "ObjectUtils", "SkApplicationInstance", "ServiceProxy", "DateUtils", "StringUtils", "NumberUtils", "MessageUtils","i18n","DatasetObserverEvents",
			function ($scope, ObjectUtils, SkApplicationInstance, ServiceProxy, DateUtils, StringUtils, NumberUtils, MessageUtils,i18n,DatasetObserverEvents) {
				var self = this;
			      
				self.dynaformID = StringUtils.nextUid();
				self.onDynaformLoad = onDynaformLoad;
				self.otherOptionsLoader = otherOptionsLoader;
				self.customTabsLoader = customTabsLoader;
				self.onClick1 = onClick1;
				self.onClick2 = onClick2;
				
				var _dsExcClassEmbarque;
				var _dsInstanciaFilha;
    
				$scope.loadByPK = loadByPK;

				init();

				function init() {
				};

				function loadByPK(objPK) {
				};

				function onDynaformLoad(dynaform, dataset) {
					
					 if (dataset.getEntityName() == 'ClientesGeovane') {
						 _dsExcClassEmbarque = dataset;
					 } else  if (dataset.getEntityName() == 'InstanciaFilha') {
						 _dsInstanciaFilha = dataset;
					 }
					 
				};

				function customTabsLoader(entityName) {
			         if (entityName == 'ClientesGeovane') {
			            var customTabs = [];
			            customTabs.push({
			               blockId: 'Teste 1',
			               description: i18n('Teste 1'),
			               controller: 'Teste1Controller',
			               controllerAs: 'ctrl',
			               templateUrl: 'html5/ExcClassEmbarque/abas/Log/Log.tpl.html'
			            },
			            {
			               blockId: 'Teste 2',
			               description: i18n('Teste'),
			               controller: 'Teste2Controller',
			               controllerAs: 'ctrl',
			               templateUrl: 'html5/ExcClassEmbarque/abas/Testes/Testes.tpl.html'
			            });
			            
			         }
			            return customTabs;
			         
			      }
				
				function otherOptionsLoader(dynaform) {
					return [
							{label: "Teste Geovane", action: lancarFin},
							{label: "Teste Geovane 2", action: lancarNota}
						];
				};

				function onClick1() {
					console.log('Click1!');
					window.alert('Click1!');
					console.log('Click1!');
				}
				
				function onClick2() {
					console.log('Click2!');
					window.alert('Click2!');
					console.log('Click2!');
				}

				function lancarFin() {

					var params = {"teste2" : "lancarFin"};

					ServiceProxy.callService("extensao@MovimentacoesSP.inserirFinanceiro2", params)
						.then(function (response) {
							 MessageUtils.showInfo("inserirFinanceiro2");
						});

				};
				
				function lancarNota() {

					var params = { "Teste1" : "lancarNota"};

					ServiceProxy.callService("extensao@MovimentacoesSP.inserirNota2", params)
						.then(function (response) {
							 MessageUtils.showInfo("inserirNota2");
						});

				};

			}]);