// 
// Decompiled by Procyon v0.5.36
// 

package br.com.geotec.appos.vc.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.CreateException;
import javax.ejb.EJBObject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jdom.Element;

import br.com.sankhya.dwf.controller.servlet.SnkDefaultServlet;
import br.com.sankhya.dwf.services.ServiceUtils;
import br.com.sankhya.modelcore.facades.CRUDServiceProvider;
import br.com.sankhya.modelcore.facades.MGEFrontFacade;
import br.com.sankhya.modelcore.facades.MGEFrontFacadeHome;
import br.com.sankhya.modulemgr.MGESession;
import br.com.sankhya.modulemgr.ModuleMgr;
import br.com.sankhya.ws.ServiceContext;
import br.com.sankhya.ws.transformer.json.Json2XMLParser;

import com.sankhya.util.RemoteEJBProxy;
import com.sankhya.util.ServiceLocator;
import com.sankhya.util.StringUtils;

public class AppOSServlet extends SnkDefaultServlet {
	private static final long			serialVersionUID				= 1L;
	@SuppressWarnings("unused")
	private static final String			SERVICE_PATH					= "br.com.sankhya.rest.services.";
	@SuppressWarnings("unused")
	private static final String			MGE_FRONT_FACADE_SESSION_KEY	= "MGEFrontFacade";
	private static final List<String>	METHOD_IGNORE;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		resp.setHeader("Access-Control-Allow-Origin", "*");
		resp.setContentType("application/json");
		resp.setCharacterEncoding("ISO-8859-1");
		final String hostName = req.getRemoteHost();
		final String ip = req.getRemoteAddr();
		Class<? extends CRUDServiceProvider> serviceClassSP = null;
		Class<?> serviceClassSPHome = null;
		EJBObject serviceEjb = null;
		final PrintWriter out = resp.getWriter();
		final HttpSession session = req.getSession();
		MGEFrontFacade facade = null;
		try {

			facade = this.getFacade(hostName, ip, req);
			ModuleMgr.getSingleton().addMgeSession(session, facade);
			session.setAttribute("usuarioLogado", (Object) facade.getAuthenticationInfo());
			final ServiceContext ctx = new ServiceContext(req);

			ctx.setTempFolder((File) this.getServletContext().getAttribute("javax.servlet.context.tempdir"));
			ctx.setAutentication((Object) facade.getAuthenticationInfo());
			ctx.makeCurrent();

			final String[] serviceName = req.getPathInfo().split("\\/");
			final String className = "AppOSSP";
			final String methodName = serviceName[1];

			serviceClassSP = (Class<? extends CRUDServiceProvider>) Class.forName("br.com.geotec.appos.services." + className);
			serviceClassSPHome = Class.forName("br.com.geotec.appos.services." + className + "Home");
			serviceEjb = (EJBObject) serviceClassSP.cast(ServiceUtils.getStatelessFacade((String) serviceClassSPHome.getDeclaredField("JNDI_NAME").get(null), (Class) serviceClassSPHome));

			if (serviceName.length == 3) {
				final Method method = serviceClassSP.getMethod(methodName, ServiceContext.class, String.class);
				final String valueParam = serviceName[2];
				method.invoke(serviceEjb, ctx, valueParam);

			} else if (serviceName.length >= 4) {
				final Method method = serviceClassSP.getMethod(methodName, ServiceContext.class, String.class, String.class);
				final String valueParam = serviceName[2];
				final String valueParam2 = serviceName[3];

				method.invoke(serviceEjb, ctx, valueParam, valueParam2);
			} else {
				final Method method = serviceClassSP.getMethod(methodName, ServiceContext.class);
				method.invoke(serviceEjb, ctx);
			}

			ctx.getHttpRequest().getSession().setAttribute("MGEFrontFacade", (Object) facade);

			resp.setStatus(ctx.getStatus());
			out.println(ctx.getJsonResponse().toString());
			// out.println(Json2XMLParser.elementToJson(responseElem));
			out.flush();

		} catch (Exception e) {
			e.printStackTrace();
			facade = this.getFacade(hostName, ip, req);
			final Element responseElem2 = this.getServiceContext(req, facade).getBodyElement();
			responseElem2.setAttribute("status", "0");

			resp.setStatus(500);

			StackTraceElement[] trace = e.getStackTrace();

			StringBuffer msg = new StringBuffer();

			for (StackTraceElement erro : trace) {
				msg.append("\nFile:" + erro.getFileName() + " Class:" + erro.getClassName() + " Method: " + erro.getMethodName() + " Line:" + Integer.toString(erro.getLineNumber()));

				if (erro.getClassName().contains("geotec.appos")) {
					break;
				}
			}

			String ret = e.toString() + " : " + e.getLocalizedMessage() + msg.toString();

			if (e.getCause() == null) {
				responseElem2.setAttribute("statusMessage", ret + ((StringUtils.getEmptyAsNull(e.getMessage()) == null) ? "Erro Interno!" : e.getMessage()));
			} else {
				responseElem2.setAttribute("statusMessage", e.getCause().getMessage());
			}
			try {
				out.println(Json2XMLParser.elementToJson(responseElem2));
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			out.flush();
		} finally {
			ServiceUtils.releaseStatelessEJB(serviceEjb);
		}
	}

	protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doPost(req, resp);
	}

	protected void doPut(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doPost(req, resp);
	}

	private ServiceContext getServiceContext(final HttpServletRequest req, final MGEFrontFacade facade) {
		try {
			final ServiceContext ctx = new ServiceContext(req);
			ctx.setTempFolder((File) this.getServletContext().getAttribute("javax.servlet.context.tempdir"));
			ctx.setAutentication((Object) facade.getAuthenticationInfo());
			ctx.makeCurrent();
			return ctx;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("rawtypes")
	private MGEFrontFacade getFacade(final String hostName, final String ip, final HttpServletRequest httpRequest) {
		final MGEFrontFacade mgeFacade = (MGEFrontFacade) httpRequest.getSession().getAttribute("MGEFrontFacade");
		if (mgeFacade == null) {
			MGEFrontFacade facade = null;
			MGESession s = null;
			try {
				s = ModuleMgr.getSingleton().getMgeSession(httpRequest.getSession().getId());
				if (s == null) {
					final MGEFrontFacadeHome home = (MGEFrontFacadeHome) ServiceLocator.getHome("mge/modelcore/ejb/session/MGEFrontFacade", (Class) MGEFrontFacadeHome.class);
					facade = home.create("SUP", (String) null, hostName, ip, false);
					facade = (MGEFrontFacade) RemoteEJBProxy.build((Class) MGEFrontFacade.class, (Object) facade);
				} else {
					facade = s.getMgeFrontFacade();
				}
				return facade;
			} catch (CreateException cEx) {
				cEx.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return mgeFacade;
	}

	static {
		(METHOD_IGNORE = new ArrayList<String>()).add("doLogin");
		AppOSServlet.METHOD_IGNORE.add("recuperarSenha");
		AppOSServlet.METHOD_IGNORE.add("saveUser");
		AppOSServlet.METHOD_IGNORE.add("isAlive");
		AppOSServlet.METHOD_IGNORE.add("doLogout");
	}
}
