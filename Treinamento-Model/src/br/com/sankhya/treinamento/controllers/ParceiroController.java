package br.com.sankhya.treinamento.controllers;

import java.math.BigDecimal;
import java.util.Collection;

import br.com.sankhya.jape.EntityFacade;
import br.com.sankhya.jape.bmp.PersistentLocalEntity;
import br.com.sankhya.jape.core.JapeSession;
import br.com.sankhya.jape.dao.JdbcWrapper;
import br.com.sankhya.jape.util.FinderWrapper;
import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.jape.vo.EntityVO;
import br.com.sankhya.jape.wrapper.JapeFactory;
import br.com.sankhya.jape.wrapper.JapeWrapper;
import br.com.sankhya.modelcore.util.DynamicEntityNames;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;
import br.com.sankhya.modelcore.util.ParameterUtils;
import br.com.sankhya.treinamento.utils.LogUtil;
import br.com.sankhya.treinamento.utils.StringUtil;

public class ParceiroController {

	public static String validaParceiro(String documentNr) throws Exception {
		String status = "PEN";

		try {

			BigDecimal codParc = gravarParceiro(documentNr);
			
			JapeWrapper clienteDAO = JapeFactory.dao("InfracomClientes");
			clienteDAO.prepareToUpdateByPK(documentNr).set("CODPARC", codParc).update();

		} catch (Exception e) {
			
			LogUtil.saveLog(e);
			
			status = "PEN";
		}

		return status;
	}
	
	@SuppressWarnings("unchecked")
	public static BigDecimal gravarParceiro(String documentNr) throws Exception {
		LogUtil.METHOD = "InfracomClientes";
		LogUtil.SYNC = "GerarParceiro";
		LogUtil.REGISTRO = documentNr;

		JapeSession.SessionHandle hnd = null;
		JdbcWrapper jdbc = null;

		BigDecimal codParc = null;

		try {
			hnd = JapeSession.open();
			JapeWrapper clienteDAO = JapeFactory.dao("InfracomClientes");
			DynamicVO clienteVO = clienteDAO.findByPK(documentNr);

			EntityFacade entityFacade = EntityFacadeFactory.getDWFFacade();
			jdbc = entityFacade.getJdbcWrapper();
			jdbc.openSession();

			DynamicVO parceiroVO = null;
			PersistentLocalEntity registroPLE = null;
			try {
				FinderWrapper finder = new FinderWrapper(DynamicEntityNames.PARCEIRO, "this.CGC_CPF = ? ", new Object[] { documentNr });
				finder.setOrderBy("this.CODPARC");
				finder.setMaxResults(1);

				
				Collection<PersistentLocalEntity> parceiroColl = entityFacade.findByDynamicFinder(finder);
				for (PersistentLocalEntity itemPLE : parceiroColl) {
					registroPLE = itemPLE;
				}

				parceiroVO = (DynamicVO) registroPLE.getValueObject();
			} catch (Exception e) {
				parceiroVO = (DynamicVO) entityFacade.getDefaultValueObjectInstance(DynamicEntityNames.PARCEIRO);
			}

			parceiroVO = getParceiroVO(clienteVO, parceiroVO, entityFacade);
			
			if (registroPLE != null && registroPLE.isValid()) {
				registroPLE.setValueObject((EntityVO) parceiroVO);
			} else {
				parceiroVO = (DynamicVO) entityFacade.createEntity(DynamicEntityNames.PARCEIRO, (EntityVO) parceiroVO).getValueObject();
			}

			//-------------------------------------------------------------------------------------------
			DynamicVO complementoVO = null;
			PersistentLocalEntity complementoPLE = null;

			try {
				complementoPLE = entityFacade.findEntityByPrimaryKey(DynamicEntityNames.COMPLEMENTO_PARCEIRO, parceiroVO.asBigDecimal("CODPARC"));

				complementoVO = (DynamicVO) complementoPLE.getValueObject();
			} catch (Exception e) {
				complementoVO = (DynamicVO) entityFacade.getDefaultValueObjectInstance(DynamicEntityNames.COMPLEMENTO_PARCEIRO);
			}

			complementoVO = getComplementoVO(clienteVO, complementoVO, entityFacade);

			if (complementoPLE != null && complementoPLE.isValid()) {
				complementoPLE.setValueObject((EntityVO) complementoVO);
			} else {
				complementoVO.setProperty("CODPARC", parceiroVO.asBigDecimal("CODPARC"));
				entityFacade.createEntity(DynamicEntityNames.COMPLEMENTO_PARCEIRO, (EntityVO) complementoVO).getValueObject();
			}

			codParc = parceiroVO.asBigDecimal("CODPARC");
			
		} catch (Exception e) {
			LogUtil.saveLog(e);
			e.printStackTrace();
		} finally {
			JdbcWrapper.closeSession(jdbc);
			JapeSession.close(hnd);
		}

		return codParc;

	}
	
	@SuppressWarnings("unchecked")
	public static void gravaContatoEntrega(String documentNR) throws Exception {
		LogUtil.METHOD = "InfracomClientes";
		LogUtil.SYNC = "GerarContato";
		LogUtil.REGISTRO = documentNR;

		JapeSession.SessionHandle hnd = null;
		JdbcWrapper jdbc = null;

		try {
			hnd = JapeSession.open();
			JapeWrapper clienteDAO = JapeFactory.dao("InfracomClientes");
			DynamicVO clienteVO = clienteDAO.findByPK(documentNR);

			if (clienteVO.asBigDecimal("CODPARC") == null) {
				return;
			}

			EntityFacade entityFacade = EntityFacadeFactory.getDWFFacade();
			jdbc = entityFacade.getJdbcWrapper();
			jdbc.openSession();

			DynamicVO novoContatoVO = (DynamicVO) entityFacade.getDefaultValueObjectInstance(DynamicEntityNames.CONTATO);

			novoContatoVO = getContatoVO(clienteVO, novoContatoVO, entityFacade);

			try {
				FinderWrapper finder = new FinderWrapper(DynamicEntityNames.CONTATO, "this.CODPARC = ? AND this.CEP LIKE ? AND this.CODEND = ? AND this.CODBAI = ? AND this.NUMEND LIKE ? AND this.NOMECONTATO LIKE ?", new Object[] { novoContatoVO.asBigDecimal("CODPARC"), novoContatoVO.asString("CEP"), novoContatoVO.asBigDecimal("CODEND"), novoContatoVO.asBigDecimal("CODBAI"), novoContatoVO.asString("NUMEND"), novoContatoVO.asString("NOMECONTATO") });
				finder.setMaxResults(1);
				
				Collection<PersistentLocalEntity> contatolColl = entityFacade.findByDynamicFinder(finder);
				for (PersistentLocalEntity itemPLE : contatolColl) {
					DynamicVO contatoVO = (DynamicVO) itemPLE.getValueObject();
					contatoVO.setProperty("COMPLEMENTO", novoContatoVO.asString("COMPLEMENTO").substring(0, 29));
					novoContatoVO = contatoVO;
					itemPLE.setValueObject((EntityVO) contatoVO);
				}

			} catch (Exception e) {
				entityFacade.createEntity(DynamicEntityNames.CONTATO, (EntityVO) novoContatoVO).getValueObject();
			}

			clienteDAO.prepareToUpdate(clienteVO).set("CODCONTATO", novoContatoVO.asBigDecimal("CODCONTATO")).update();;

		} catch (Exception e) {
			LogUtil.saveLog(e);
			e.printStackTrace();
		} finally {
			JdbcWrapper.closeSession(jdbc);
			JapeSession.close(hnd);
		}

	}

	private static DynamicVO getContatoVO(DynamicVO clienteVO, DynamicVO contatoVO, EntityFacade entityFacade) throws Exception {
		DynamicVO endEnderecoVO = getEnderecoVO(clienteVO.asString("DOCUMENTNR"), entityFacade);

		//---Enderešos
		contatoVO.setProperty("CODPARC", clienteVO.asBigDecimal("CODPARC"));
		contatoVO.setProperty("NOMECONTATO", endEnderecoVO.asString("RECIPIENTNM"));
		contatoVO.setProperty("CODCID", EnderecoController.tratarCidade(endEnderecoVO.asString("STATE"), endEnderecoVO.asString("CITY"), endEnderecoVO.asString("POSTALCD"), entityFacade));
		contatoVO.setProperty("CODBAI", EnderecoController.tratarBairro(endEnderecoVO.asString("QUARTER")));
		contatoVO.setProperty("CODEND", EnderecoController.tratarEndereco(null, endEnderecoVO.asString("ADDRESS")));
		contatoVO.setProperty("NUMEND", endEnderecoVO.asString("ADDRESSNR"));
		contatoVO.setProperty("COMPLEMENTO", endEnderecoVO.asString("ADDITIONALINFO"));
		contatoVO.setProperty("CEP", StringUtil.retornaNumeros(endEnderecoVO.asString("POSTALCD")));

		return contatoVO;
	}

	private static DynamicVO getComplementoVO(DynamicVO clienteVO, DynamicVO complementoVO, EntityFacade entityFacade) throws Exception {
		DynamicVO endEnderecoVO = getEnderecoVO(clienteVO.asString("DOCUMENTNR"), entityFacade);

		String complemento = endEnderecoVO.asString("ADDITIONALINFO");

		String numEnd = endEnderecoVO.asString("ADDRESSNR");

		if (numEnd != null && numEnd.length() > 6) {
			numEnd = numEnd.substring(0, 5);
		}

		if (complemento != null && complemento.length() > 30) {
			complemento = complemento.substring(0, 29);
		}
		//---Enderešos
		complementoVO.setProperty("CODCIDENTREGA", EnderecoController.tratarCidade(endEnderecoVO.asString("STATE"), endEnderecoVO.asString("CITY"), endEnderecoVO.asString("POSTALCD"), entityFacade));
		complementoVO.setProperty("CODBAIENTREGA", EnderecoController.tratarBairro(endEnderecoVO.asString("QUARTER")));
		complementoVO.setProperty("CODENDENTREGA", EnderecoController.tratarEndereco(null, endEnderecoVO.asString("ADDRESS")));
		complementoVO.setProperty("NUMENTREGA", numEnd);
		complementoVO.setProperty("COMPLENTREGA", complemento);
		complementoVO.setProperty("CEPENTREGA", StringUtil.retornaNumeros(endEnderecoVO.asString("POSTALCD")));

		return complementoVO;

	}

	private static DynamicVO getParceiroVO(DynamicVO clienteVO, DynamicVO parceiroVO, EntityFacade entityFacade) throws Exception {
		if (parceiroVO.asBigDecimal("CODPARC") == null) {
			BigDecimal codParcMod = (BigDecimal) ParameterUtils.getParameter("IFCCODPARCMOD");

			if (codParcMod != null && codParcMod.intValue() > 0) {
				JapeWrapper parceiroDAO = JapeFactory.dao(DynamicEntityNames.PARCEIRO);

				DynamicVO parceiroModeloVO = parceiroDAO.findByPK(codParcMod);

				if (parceiroModeloVO != null) {
					parceiroVO = parceiroModeloVO;
					parceiroVO.setProperty("CODPARC", null);
				}
			}

		}

		DynamicVO endCobrancaVO = getEnderecoVO(clienteVO.asString("DOCUMENTNR"), entityFacade);

		String nome = clienteVO.asString("NAME");
		String cgcCPF = clienteVO.asString("DOCUMENTNR");
		String inscrRg = clienteVO.asString("STATESUBSCRIPTION");
		String telefone = clienteVO.asString("PHONE1");

		String sexo = "M";
		String razaoSocial = nome;

		parceiroVO.setProperty("NOMEPARC", nome);
		parceiroVO.setProperty("TIPPESSOA", "J");
		parceiroVO.setProperty("RAZAOSOCIAL", razaoSocial);
		parceiroVO.setProperty("CGC_CPF", cgcCPF);
		parceiroVO.setProperty("IDENTINSCESTAD", inscrRg);
		parceiroVO.setProperty("CLIENTE", "S");
		parceiroVO.setProperty("SEXO", sexo);
		parceiroVO.setProperty("EMAIL", clienteVO.asString("EMAIL"));
		parceiroVO.setProperty("EMAILNFE", clienteVO.asString("EMAIL"));
		parceiroVO.setProperty("AD_EMAILFIN", clienteVO.asString("EMAIL"));
		parceiroVO.setProperty("FAX", StringUtil.trataTelefone(clienteVO.asString("PHONE2")));
		parceiroVO.setProperty("TELEFONE", StringUtil.trataTelefone(telefone));
		parceiroVO.setProperty("CODREG", BigDecimal.ZERO);
	//	parceiroVO.setProperty("DTCAD", StringUtil.toTimestamp(clienteVO.asString("CREATEDATE"), "yyyy-MM-dd'T'hh:mm:ss"));
		//parceiroVO.setProperty("DTNASC", new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(pedidoVO.asString("DATANASCIMENTO")).getTime()));		

		//---Enderešos

		String numEnd = endCobrancaVO.asString("ADDRESSNR");

		if (numEnd != null && numEnd.length() > 6) {
			numEnd = numEnd.substring(0, 5);
		}

		String complemento = endCobrancaVO.asString("ADDITIONALINFO");

		if (complemento != null && complemento.length() > 30) {
			complemento = complemento.substring(0, 29);
		}

		parceiroVO.setProperty("CODCID", EnderecoController.tratarCidade(endCobrancaVO.asString("STATE"), endCobrancaVO.asString("CITY"), endCobrancaVO.asString("POSTALCD"), entityFacade));
		parceiroVO.setProperty("CODBAI", EnderecoController.tratarBairro(endCobrancaVO.asString("QUARTER")));
		parceiroVO.setProperty("CODEND", EnderecoController.tratarEndereco(null, endCobrancaVO.asString("ADDRESS")));
		parceiroVO.setProperty("NUMEND", numEnd);
		parceiroVO.setProperty("COMPLEMENTO", complemento);
		parceiroVO.setProperty("CEP", StringUtil.retornaNumeros(endCobrancaVO.asString("POSTALCD")));

		return parceiroVO;
	}

	private static DynamicVO getEnderecoVO(String documentNr, EntityFacade entityFacade) throws Exception {
		JapeWrapper enderecoDAO = JapeFactory.dao("InfracomEndClientes");

		DynamicVO enderecoVO = enderecoDAO.findOne("this.DOCUMENTNR = ?", documentNr);

		return enderecoVO;
	}

}
