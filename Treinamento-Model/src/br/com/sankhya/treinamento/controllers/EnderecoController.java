package br.com.sankhya.treinamento.controllers;

import java.math.BigDecimal;

import br.com.sankhya.jape.EntityFacade;
import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.jape.wrapper.JapeFactory;
import br.com.sankhya.jape.wrapper.JapeWrapper;
import br.com.sankhya.modelcore.util.DynamicEntityNames;
import br.com.sankhya.modelcore.util.EnderecoUtils;
import br.com.sankhya.treinamento.utils.StringUtil;
import br.com.snkcps.viacep.CepHellper;

public class EnderecoController {
	public static BigDecimal tratarCidade(String siglaUF, String nomeCidade, String cep, EntityFacade entityFacade) throws Exception {
		BigDecimal codUF = getCodigoEstado(siglaUF);

		BigDecimal codCid = EnderecoUtils.findCodigoCidadeByDescriptionAndUF(nomeCidade, codUF, entityFacade, StringUtil.retornaNumeros(cep));

		if (codCid == null || codCid == BigDecimal.ZERO) {
			String codIbge = null;
			try {
				codIbge = CepHellper.buscaIbgePorCep(cep);
			} catch (Exception e) {
				throw new Exception("Erro no servi�o de busca de cep:" + nomeCidade + "/" + siglaUF);
			}

			JapeWrapper cidadeDAO = JapeFactory.dao(DynamicEntityNames.CIDADE);
			DynamicVO cidadeVO = cidadeDAO.findOne("this.CODMUNFIS = ?", new BigDecimal(codIbge));

			if (cidadeVO == null) {
				throw new Exception("Cidade n�o cadastrada:" + nomeCidade + "/" + siglaUF + " Cod. Ibge:" + codIbge);

			}

			codCid = cidadeVO.asBigDecimal("CODCID");

		}

		return codCid;
	}

	private static BigDecimal getCodigoEstado(String siglaUF) throws Exception {
		JapeWrapper ufDAO = JapeFactory.dao(DynamicEntityNames.UNIDADE_FEDERATIVA);

		DynamicVO ufVO = ufDAO.findOne("this.UF = ?", new Object[] { siglaUF });

		return ufVO.asBigDecimal("CODUF");
	}

	public static BigDecimal tratarBairro(String nomeBairro) throws Exception {
		nomeBairro = nomeBairro.trim();
		JapeWrapper bairroDAO = JapeFactory.dao(DynamicEntityNames.BAIRRO);
		DynamicVO bairroVO = bairroDAO.findOne("UPPER(this.NOMEBAI) like ?", nomeBairro.toUpperCase());

		if (bairroVO == null) {
			bairroVO = bairroDAO.create().set("NOMEBAI", nomeBairro).save();
		}

		BigDecimal codBai = bairroVO.asBigDecimal("CODBAI");

		return codBai;
	}

	public static BigDecimal tratarEndereco(String tipoEndereco, String nomeEndereco) throws Exception {
		JapeWrapper enderecoDAO = JapeFactory.dao(DynamicEntityNames.ENDERECO);
		
		nomeEndereco = nomeEndereco.trim();
		
		if (nomeEndereco.length() > 60) {
			nomeEndereco = nomeEndereco.substring(0, 59);
		}

		if (tipoEndereco == null) {
			tipoEndereco = "Rua";
		}
		
		nomeEndereco = nomeEndereco.replaceFirst(tipoEndereco, "").trim();
		

		DynamicVO enderecoVO = enderecoDAO.findOne("UPPER(this.NOMEEND) like ? AND this.TIPO = ?", new Object[] { nomeEndereco.toUpperCase(), tipoEndereco });

		if (enderecoVO == null) {
			enderecoVO = enderecoDAO.create().set("NOMEEND", nomeEndereco).set("TIPO", tipoEndereco).save();
		}

		BigDecimal codEnd = enderecoVO.asBigDecimal("CODEND");

		return codEnd;
	}
}
