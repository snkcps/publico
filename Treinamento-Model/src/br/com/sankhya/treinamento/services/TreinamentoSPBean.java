package br.com.sankhya.treinamento.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import br.com.sankhya.jape.EntityFacade;
import br.com.sankhya.jape.bmp.PersistentLocalEntity;
import br.com.sankhya.jape.core.JapeSession;
import br.com.sankhya.jape.core.JapeSession.SessionHandle;
import br.com.sankhya.jape.dao.JdbcWrapper;
import br.com.sankhya.jape.util.FinderWrapper;
import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.jape.vo.EntityVO;
import br.com.sankhya.jape.wrapper.JapeFactory;
import br.com.sankhya.jape.wrapper.JapeWrapper;
import br.com.sankhya.jape.wrapper.fluid.FluidCreateVO;
import br.com.sankhya.jape.wrapper.fluid.FluidUpdateVO;
import br.com.sankhya.modelcore.MGEModelException;
import br.com.sankhya.modelcore.auth.AuthenticationInfo;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;
import br.com.sankhya.modelcore.util.SPBeanUtils;
import br.com.sankhya.treinamento.controllers.ParceiroController;
import br.com.sankhya.treinamento.parsers.PedidoParser;
import br.com.sankhya.treinamento.utils.JapeContext;
import br.com.sankhya.treinamento.utils.LogDetalhe;
import br.com.sankhya.ws.ServiceContext;

import com.sankhya.util.BigDecimalUtil;

/**
 * @author Geovane O. Gadioli
 * @ejb.bean name="TreinamentoSP" jndi-name="br/com/sankhya/treinamento/services/TreinamentoSP" type="Stateless" transaction-type="Container" view-type="remote"
 * @ejb.transaction type="Supports"
 * @ejb.util generate="false"
 */

public class TreinamentoSPBean implements SessionBean {
	private static final long	serialVersionUID	= 1L;
	protected SessionContext	context;

	/**
	 * @ejb.interface-method tview-type="remote"
	 * @ejb.transaction type="Required"
	 */

	@SuppressWarnings("unchecked")
	public void postClientes(ServiceContext ctx) throws Exception {

		final AuthenticationInfo authInfo = new AuthenticationInfo("SUP", BigDecimalUtil.ZERO_VALUE, BigDecimalUtil.ZERO_VALUE, new Integer(Integer.MAX_VALUE));
		authInfo.makeCurrent();

		SessionHandle hnd = null;
		String response = "";

		try {

			hnd = JapeSession.open();
			JapeContext.setup(authInfo);
		
			BufferedReader in = new BufferedReader(new InputStreamReader(ctx.getRequestInputStream(),"UTF-8"));
			String inputLine;

			StringBuilder responseBody = new StringBuilder();

			while ((inputLine = in.readLine()) != null) {
				responseBody.append(inputLine);
			}
			in.close();

			String bodyString = responseBody.toString();

			SAXBuilder saxBuilder = new SAXBuilder();
			Document doc = saxBuilder.build(new StringReader(bodyString.replaceAll("&", "e")));

			Element xml = doc.getRootElement();

			Element body = xml.getChild("Body", xml.getNamespace());
			Element notify = body.getChild("notifyCustomerCreationRequest", xml.getNamespace("cus"));
			Element customer = notify.getChild("customer", xml.getNamespace("cli"));
			String name = customer.getChild("name", xml.getNamespace("cli")).getText();
			String email = customer.getChild("email", xml.getNamespace("cli")).getText();
			String documentNr = customer.getChild("documentNr", xml.getNamespace("cli")).getText();
			String createDate = customer.getChild("createDate", xml.getNamespace("cli")).getText();
			String stateSubscription = customer.getChild("stateSubscription", xml.getNamespace("cli")).getText();
			String representativeNm = customer.getChild("representativeNm", xml.getNamespace("cli")).getText();

			List<Element> phoneList = customer.getChild("phoneList", xml.getNamespace("cli")).getChildren("phone", xml.getNamespace("cli"));

			String phoneComercial = "";
			String phoneCelular = "";

			for (int i = 0; i < phoneList.size(); i++) {
				Element phone = phoneList.get(i);
				String phoneTp = phone.getChild("phoneTp", xml.getNamespace("cli")).getText();
				String areaCd = phone.getChild("areaCd", xml.getNamespace("cli")).getText();
				String phoneNr = phone.getChild("phoneNr", xml.getNamespace("cli")).getText();

				if (phoneTp.equals("1")) {
					phoneComercial = areaCd + phoneNr;
				} else if (phoneTp.equals("2")) {
					phoneCelular = areaCd + phoneNr;
				}

				// System.out.println("phone:" + phone.getChildText("phoneNr", xml.getNamespace("cli")));
			}

			JapeWrapper cliDAO = JapeFactory.dao("InfracomClientes");
			DynamicVO clienteVO = cliDAO.findByPK(documentNr);

			XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
			String xmlPretty = xmlOutputter.outputString(doc);

			if (clienteVO == null) {
				FluidCreateVO createVO = cliDAO.create();
				createVO.set("XML", xmlPretty.toCharArray());
				createVO.set("NAME", name);
				createVO.set("EMAIL", email);
				createVO.set("DOCUMENTNR", documentNr);
				createVO.set("CREATEDATE", createDate);
				createVO.set("STATESUBSCRIPTION", stateSubscription);
				createVO.set("REPRESENTATIVENM", representativeNm);
				createVO.set("PHONE1", phoneComercial);
				createVO.set("PHONE2", phoneCelular);
				clienteVO = createVO.save();
			} else {
				FluidUpdateVO updateVO = cliDAO.prepareToUpdate(clienteVO);
				updateVO.set("XML", xmlPretty.toCharArray());
				updateVO.set("NAME", name);
				updateVO.set("EMAIL", email);
				updateVO.set("DOCUMENTNR", documentNr);
				updateVO.set("CREATEDATE", createDate);
				updateVO.set("STATESUBSCRIPTION", stateSubscription);
				updateVO.set("REPRESENTATIVENM", representativeNm);
				updateVO.set("PHONE1", phoneComercial);
				updateVO.set("PHONE2", phoneCelular);
				updateVO.update();
			}

			JapeWrapper endDAO = JapeFactory.dao("InfracomEndClientes");
			endDAO.deleteByCriteria("DOCUMENTNR = ?", documentNr);

			List<Element> addressList = customer.getChild("addressList", xml.getNamespace("cli")).getChildren("address", xml.getNamespace("cli"));

			for (int i = 0; i < addressList.size(); i++) {
				Element addressE = addressList.get(i);
				String recipientNm = addressE.getChild("recipientNm", xml.getNamespace("cli")).getText();
				String address = addressE.getChild("address", xml.getNamespace("cli")).getText();
				String addressNr = addressE.getChild("addressNr", xml.getNamespace("cli")).getText();
				String additionalInfo = addressE.getChild("additionalInfo", xml.getNamespace("cli")).getText();
				String quarter = addressE.getChild("quarter", xml.getNamespace("cli")).getText();
				String city = addressE.getChild("city", xml.getNamespace("cli")).getText();
				String state = addressE.getChild("state", xml.getNamespace("cli")).getText();
				String postalCd = addressE.getChild("postalCd", xml.getNamespace("cli")).getText();

				FluidCreateVO createEndVO = endDAO.create();
				createEndVO.set("DOCUMENTNR", clienteVO.asString("DOCUMENTNR"));
				createEndVO.set("RECIPIENTNM", recipientNm);
				createEndVO.set("ADDRESS", address);
				createEndVO.set("ADDRESSNR", addressNr);
				createEndVO.set("ADDITIONALINFO", additionalInfo);
				createEndVO.set("QUARTER", quarter);
				createEndVO.set("CITY", city);
				createEndVO.set("STATE", state);
				createEndVO.set("POSTALCD", postalCd);
				createEndVO.save();

				// System.out.println("phone:" + phone.getChildText("phoneNr", xml.getNamespace("cli")));
			}

			String status = ParceiroController.validaParceiro(clienteVO.asString("DOCUMENTNR"));

			ctx.getBodyElement().removeContent();

			response = "<soapenv:Envelope xmlns:cus=\"http://www.accurate.com/acec/CustomerServices\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n" + "	<soapenv:Header/>\r\n" + "	<soapenv:Body>\r\n" + "		<cus:notifyCustomerCreationResponse>\r\n" + "			<cus:status>" + status + "</cus:status>\r\n" + "		</cus:notifyCustomerCreationResponse>\r\n" + "	</soapenv:Body>\r\n" + "</soapenv:Envelope>";

			ctx.setStatus(201);

		} catch (Exception e) {
			String log = LogDetalhe.detalhaErro(e, "snkcps.");

			response = "Erro: " + log;

			ctx.setStatus(500);
		} finally {
			;
			ctx.setResponseData(response);
			JapeSession.close(hnd);
		}
	}

	/**
	 * @ejb.interface-method tview-type="remote"
	 * @ejb.transaction type="Required"
	 */

	public void postPedidos(ServiceContext ctx) throws Exception {
		final AuthenticationInfo authInfo = new AuthenticationInfo("SUP", BigDecimalUtil.ZERO_VALUE, BigDecimalUtil.ZERO_VALUE, new Integer(Integer.MAX_VALUE));
		authInfo.makeCurrent();

		SessionHandle hnd = null;
		String response = "";
		JdbcWrapper jdbc = null;

		try {

			hnd = JapeSession.open();
			JapeContext.setup(authInfo);

			BufferedReader in = new BufferedReader(new InputStreamReader(ctx.getRequestInputStream(),"UTF-8"));
			String inputLine;

			StringBuilder responseBody = new StringBuilder();

			while ((inputLine = in.readLine()) != null) {
				responseBody.append(inputLine);
			}
			in.close();

			String bodyString = responseBody.toString();

			SAXBuilder saxBuilder = new SAXBuilder();
			Document doc = saxBuilder.build(new StringReader(bodyString));

			Element xml = doc.getRootElement();

			XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
			String xmlPretty = xmlOutputter.outputString(doc);

			Element body = xml.getChild("Body", xml.getNamespace());
			Element notify = body.getChild("integrateOrderRequest", xml.getNamespace("boor"));
			Element order = notify.getChild("order", xml.getNamespace("ord"));
			String orderId = order.getChild("orderId", xml.getNamespace("ord")).getText();

			final EntityFacade dwf = EntityFacadeFactory.getDWFFacade();
			jdbc = dwf.getJdbcWrapper();
			jdbc.openSession();

			PersistentLocalEntity registroPLE = null;
			DynamicVO pedidoVO = null;

			try {
				registroPLE = dwf.findEntityByPrimaryKey("InfracomPedidos", orderId);
				pedidoVO = (DynamicVO) registroPLE.getValueObject();
			} catch (Exception e) {
				pedidoVO = (DynamicVO) dwf.getDefaultValueObjectInstance("InfracomPedidos");
			}

			pedidoVO = PedidoParser.parse(xml, pedidoVO);
			pedidoVO.setProperty("XML", xmlPretty.toCharArray());

			if (registroPLE == null)
				dwf.createEntity("InfracomPedidos", (EntityVO) pedidoVO);
			else
				registroPLE.setValueObject((EntityVO) pedidoVO);

			FinderWrapper finder = new FinderWrapper("InfracomItens", "this.ORDERID = " + orderId);
			dwf.removeByCriteria(finder);
			// Gravar Itens

			ArrayList<DynamicVO> itens = PedidoParser.parseItens(xml, dwf);
			
			for(DynamicVO itemVO : itens){
				dwf.createEntity("InfracomItens", (EntityVO) itemVO);
			}
			
			
			ctx.getBodyElement().removeContent();

			if (registroPLE == null)
				response = "<env:Envelope xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\">" + "   <env:Body>" + "         <integrateOrderResponse xmlns=\"http://www.accurate.com/acec/AcecBOSOAIntegration/BOOrderIntegration\">" + "	<status>OK</status>" + "	<message>Pedido integrado com sucesso</message>" + "         </integrateOrderResponse>" + "   </env:Body>" + "</env:Envelope>";
			else
				response = "<env:Envelope xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\">" + "   <env:Body>" + "         <integrateOrderResponse xmlns=\"http://www.accurate.com/acec/AcecBOSOAIntegration/BOOrderIntegration\">" + "	<status>OK</status>" + "	<message>Pedido j� existe no ERP</message>" + "         </integrateOrderResponse>" + "   </env:Body>" + "</env:Envelope>";

			ctx.setStatus(201);

		} catch (Exception e) {
			String log = LogDetalhe.detalhaErro(e, "snkcps.");

			response = "Erro: " + log;

			ctx.setStatus(500);
		} finally {
			;
			ctx.setResponseData(response);
			JapeSession.close(hnd);
		}
	}

	/**
	 * @ejb.interface-method tview-type="remote"
	 * @ejb.transaction type="Required"
	 */

	public void postPagamentos(ServiceContext ctx) throws Exception {

		final AuthenticationInfo authInfo = new AuthenticationInfo("SUP", BigDecimalUtil.ZERO_VALUE, BigDecimalUtil.ZERO_VALUE, new Integer(Integer.MAX_VALUE));
		authInfo.makeCurrent();

		SessionHandle hnd = null;
		String response = "";

		try {

			hnd = JapeSession.open();
			JapeContext.setup(authInfo);

			BufferedReader in = new BufferedReader(new InputStreamReader(ctx.getRequestInputStream(),"UTF-8"));
			String inputLine;

			StringBuilder responseBody = new StringBuilder();

			while ((inputLine = in.readLine()) != null) {
				responseBody.append(inputLine);
			}
			in.close();

			String bodyString = responseBody.toString();

			SAXBuilder saxBuilder = new SAXBuilder();
			Document doc = saxBuilder.build(new StringReader(bodyString));

			Element xml = doc.getRootElement();

			Element body = xml.getChild("Body", xml.getNamespace());
			Element confirmPayment = body.getChild("confirmPaymentRequest", xml.getNamespace("pay"));
			String orderId = confirmPayment.getChild("orderId", xml.getNamespace("pay")).getText();
			String status = confirmPayment.getChild("status", xml.getNamespace("pay")).getText();

			JapeWrapper pedidoDAO = JapeFactory.dao("InfracomPedidos");
			DynamicVO pedidoVO = pedidoDAO.findByPK(orderId);

			if (pedidoVO == null) {
				response = "<env:Envelope xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">" + "   <env:Body>" + "      <confirmPaymentResponse xmlns=\"http://www.accurate.com/acec/PaymentServices\">" + "         <success>false</success>" + "      </confirmPaymentResponse>" + "   </env:Body>" + "</env:Envelope>";

				ctx.setStatus(400);

			} else {
				FluidUpdateVO updateVO = pedidoDAO.prepareToUpdate(pedidoVO);
				updateVO.set("PAYMENTSTATUS", status);
				updateVO.update();

				response = "<env:Envelope xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">" + "   <env:Body>" + "      <confirmPaymentResponse xmlns=\"http://www.accurate.com/acec/PaymentServices\">" + "         <success>true</success>" + "      </confirmPaymentResponse>" + "   </env:Body>" + "</env:Envelope>";

				ctx.setStatus(200);
			}

			ctx.getBodyElement().removeContent();

		} catch (Exception e) {
			String log = LogDetalhe.detalhaErro(e, "snkcps.");

			response = "Erro: " + log;

			ctx.setStatus(500);
		} finally {
			;
			ctx.setResponseData(response);
			JapeSession.close(hnd);
		}
	}

	protected void setupContext(ServiceContext ctx) throws Exception {
		SPBeanUtils.setupContext(ctx);
	}

	protected void throwExceptionRollingBack(Throwable e) throws MGEModelException {
		SPBeanUtils.throwExceptionRollingBack(e, context);
	}

	@Override
	public void setSessionContext(SessionContext ctx) throws EJBException, RemoteException {
		this.context = ctx;
	}

	@Override
	public void ejbActivate() throws EJBException, RemoteException {
	}

	@Override
	public void ejbPassivate() throws EJBException, RemoteException {
	}

	@Override
	public void ejbRemove() throws EJBException, RemoteException {
	}
}
