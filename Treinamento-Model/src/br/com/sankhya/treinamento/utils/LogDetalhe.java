package br.com.sankhya.treinamento.utils;

public class LogDetalhe {
	public static String detalhaErro(Exception e, String pkg) {

		StackTraceElement[] trace = e.getStackTrace();

		StringBuffer msg = new StringBuffer();

		for (StackTraceElement erro : trace) {
			msg.append( "\nFile:" + erro.getFileName() + " Class:" + erro.getClassName() + " Method: " + erro.getMethodName() + " Line:" + Integer.toString(erro.getLineNumber()));

			if (erro.getClassName().contains(pkg)) {
				break;
			}
		}

		String log = e.getMessage() + " / "+ e.getLocalizedMessage() + " / "+ msg.toString();

		return log;
	}
}
