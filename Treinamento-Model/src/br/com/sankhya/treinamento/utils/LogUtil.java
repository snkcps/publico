package br.com.sankhya.treinamento.utils;

import br.com.sankhya.jape.EntityFacade;
import br.com.sankhya.jape.core.JapeSession;
import br.com.sankhya.jape.core.JapeSession.SessionHandle;
import br.com.sankhya.jape.dao.JdbcWrapper;
import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.jape.vo.EntityVO;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;

public class LogUtil {
	public static String	SYNC;
	public static String	URL;
	public static String	REGISTRO;
	public static String	METHOD;
	public static String	REQUEST;
	public static String	RESPONSE;

	public static void saveLog(Exception e) throws Exception {

		StackTraceElement[] trace = e.getStackTrace();
		String error = e.toString();
		
		StringBuffer msg = new StringBuffer();
		
		for (StackTraceElement erro : trace){
			msg.append("\nFile:" + erro.getFileName() + " Class:" + erro.getClassName() + " Method: " + erro.getMethodName() + " Line:" + Integer.toString(erro.getLineNumber()));
			
			if(erro.getClassName().contains("snkcps")){
				break;
			}
		}
		
		String log = e.getMessage() + msg.toString();
		JdbcWrapper jdbc = null;
		SessionHandle hnd = JapeSession.open();

		final EntityFacade dwfEntityFacade = EntityFacadeFactory.getDWFFacade();
		jdbc = dwfEntityFacade.getJdbcWrapper();
		jdbc.openSession();
		/*
		if (REQUEST != null) {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			JsonParser jp = new JsonParser();
			JsonElement je = jp.parse(REQUEST);
			REQUEST = gson.toJson(je);
		}
		*/
		try {
			DynamicVO logVO = (DynamicVO) dwfEntityFacade.getDefaultValueObjectInstance("AD_IFCLOG");

			logVO.setProperty("SYNC", SYNC);
			logVO.setProperty("URL", URL);
			logVO.setProperty("REGISTRO", REGISTRO);
			logVO.setProperty("METHOD", METHOD);
			logVO.setProperty("ERROR", error.substring(0, (error.length() > 100 ? 99 : error.length() - 1)));

			if (log != null) {
				logVO.setProperty("LOG", log.toCharArray());
			}

			if (REQUEST != null) {
				logVO.setProperty("REQUEST", REQUEST.toCharArray());
			}

			if (RESPONSE != null) {
				logVO.setProperty("RESPONSE", RESPONSE.toCharArray());
			}
			
			dwfEntityFacade.createEntity("AD_IFCLOG", (EntityVO) logVO);
		} catch (Exception err) {
			throw err;
		} finally {
			JdbcWrapper.closeSession(jdbc);
			JapeSession.close(hnd);
		}

		
		
	}

}
