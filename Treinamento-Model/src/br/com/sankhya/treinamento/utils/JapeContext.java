package br.com.sankhya.treinamento.utils;

import java.sql.Timestamp;

import javax.ejb.SessionContext;

import br.com.sankhya.jape.util.JapeSessionContext;
import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.modelcore.MGEModelException;
import br.com.sankhya.modelcore.auth.AuthenticationInfo;
import br.com.sankhya.modelcore.dwfdata.vo.tsi.UsuarioVO;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;

import com.sankhya.util.TimeUtils;

public class JapeContext
{
	@SuppressWarnings("rawtypes")
    public static void setup(final AuthenticationInfo authInfo) throws Exception {

		
		final UsuarioVO usuVO = (UsuarioVO)((DynamicVO)EntityFacadeFactory.getDWFFacade().findEntityByPrimaryKeyAsVO("Usuario", new Object[] { authInfo.getUserID() })).wrapInterface((Class)UsuarioVO.class);
        JapeSessionContext.putProperty("usuario_logado", (Object)authInfo.getUserID());
        JapeSessionContext.putProperty("emp_usu_logado", (Object)usuVO.getCODEMP());
        JapeSessionContext.putProperty("dh_atual", (Object)new Timestamp(System.currentTimeMillis()));
        JapeSessionContext.putProperty("d_atual", (Object)new Timestamp(TimeUtils.getToday()));
        JapeSessionContext.putProperty("usuarioVO", (Object)usuVO);
        JapeSessionContext.putProperty("authInfo", (Object)authInfo);
    }
    
    public static void throwExceptionRollingBack(final Throwable e, final SessionContext context) throws MGEModelException {
        e.printStackTrace();
        context.setRollbackOnly();
        MGEModelException.throwMe(e);
    }
}
