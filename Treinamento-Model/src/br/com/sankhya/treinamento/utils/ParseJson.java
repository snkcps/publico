package br.com.sankhya.treinamento.utils;

import java.util.Collection;
import java.util.Map;

import br.com.sankhya.jape.dao.EntityDAO;
import br.com.sankhya.jape.dao.EntityPropertyDescriptor;
import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.modelcore.util.DynamicEntityNames;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ParseJson {
	
	public static JsonObject retornaJson(EntityDAO tabDAO, DynamicVO entityVO) throws Exception {

		@SuppressWarnings("unchecked")
		Map<String, EntityPropertyDescriptor> allFields = tabDAO.getSQLProvider().getAllFieldsByName();

		JsonObject object = new JsonObject();

		for (String fieldName : allFields.keySet()) {

			String value = tabDAO.getFieldAsString(entityVO.getProperty(fieldName), fieldName);
			object.addProperty(fieldName.toLowerCase(), value);
		}

		return object;
	}
	
	public static String retornaJsonString(EntityDAO tabDAO, DynamicVO entityVO) throws Exception {
		return retornaJson( tabDAO, entityVO).toString();
	}

	
	public static JsonArray retornaJson(EntityDAO tabDAO, Collection<DynamicVO> listVO) throws Exception {

		JsonArray array = new JsonArray();

		for (DynamicVO entityVO : listVO) {
			array.add(retornaJson(tabDAO, entityVO));
		}
		return array;
	}
	
	public static String retornaJsonString(EntityDAO tabDAO, Collection<DynamicVO> listVO) throws Exception {
		return retornaJson( tabDAO, listVO).toString();
	}

	public static JsonElement toOSJson(EntityDAO tabDAO, Collection<DynamicVO> listVO) throws Exception {
		JsonArray array = new JsonArray();

		for (DynamicVO itemVO : listVO) {			
			JsonObject os = retornaJson(tabDAO, itemVO);
			
			DynamicVO osVO = itemVO.asDymamicVO(DynamicEntityNames.ORDEM_SERVICO);
			
			os.addProperty("nomeparc", osVO.asDymamicVO(DynamicEntityNames.PARCEIRO).asString("NOMEPARC"));
			os.addProperty("descricao", osVO.asString("DESCRICAO"));
			os.addProperty("nomecontato", osVO.asString("NOMECONTATO"));
			os.addProperty("dhchamada",  StringUtil.toString(osVO.asTimestamp("DHCHAMADA"), "yyyy-MM-dd") );
			os.addProperty("dtfechamento", StringUtil.toString(osVO.asTimestamp("DTFECHAMENTO"), "yyyy-MM-dd"));
			os.addProperty("situacao", osVO.asString("SITUACAO"));
			os.addProperty("endereco", osVO.asString("ENDERECO"));
			os.addProperty("complemento", osVO.asString("COMPLEMENTO"));
			os.addProperty("bairro", osVO.asString("BAIRRO"));
			os.addProperty("cidade", osVO.asString("CIDADE"));
			
			array.add(os);
		}
		return array;
	}

}
