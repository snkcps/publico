package br.com.sankhya.treinamento.utils;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import br.com.sankhya.jape.core.JapeSession;
import br.com.sankhya.modelcore.comercial.BoletoHelper;

import com.google.api.client.util.Base64;

public class ReportUtils {
	public static JasperPrint juntar(final List<JasperPrint> lista, final BigDecimal nuNota) {
		if (lista.isEmpty()) {
			return null;
		}
		final JasperPrint resultado = lista.iterator().next();
		final Object[] jaspersArray = lista.toArray();
		if (nuNota != null) {
			resultado.setName("Impressao_Nota_Boleto_" + nuNota);
		}
		for (int i = 1; i < lista.size(); ++i) {
			final JasperPrint corrente = (JasperPrint) jaspersArray[i];
			@SuppressWarnings("unchecked")
			final List<JRPrintPage> pages = (List<JRPrintPage>) corrente.getPages();
			for (final JRPrintPage page : pages) {
				resultado.addPage(page);
			}
		}
		return resultado;
	}

	public static String paraBase64(final JasperPrint jp) throws JRException {
		if (jp == null) {
			return null;
		}
		return new String(Base64.encodeBase64String(JasperExportManager.exportReportToPdf(jp)));
	}

	public static String juntarTransformarB64(final List<JasperPrint> lista) throws JRException {
		if (lista.isEmpty()) {
			return null;
		}
		final String resposta = paraBase64(juntar(lista, null));
		return resposta;
	}
	
	public String getBoletoFinanceiro(final Collection<BigDecimal> financeirosSelecionados) throws Exception {
		JapeSession.SessionHandle hnd = null;
     
        try {
            hnd = JapeSession.open();
            final BoletoHelper boletoHelper = new BoletoHelper();
            final BoletoHelper.ConfiguracaoBoleto boletoConfig = new BoletoHelper.ConfiguracaoBoleto();
            boletoConfig.setTipoSaidaBoleto(1);
            boletoConfig.setUsaContaBcoFinanceiros(false);
            boletoConfig.setGerarNumeroBoleto(false);
            boletoConfig.setReimprimirBoleta(false);
            boletoConfig.setTipoReimpressao("S");
            boletoConfig.setFinanceirosSelecionados(financeirosSelecionados);
            
            if (!JapeSession.getCurrentSession().hasTransaction()){
            	hnd.beginTransaction();
            }
           
            boletoHelper.gerarBoleto(boletoConfig);
            final byte[] boleto = boletoHelper.getBoletosPDF();
             
            return Base64.encodeBase64String(boleto);
        }
        catch (Exception e) {
            throw e;
        }
        finally {
            JapeSession.close(hnd);
        }
    }
	
	public static String getBoleto(final Collection<BigDecimal> financeirosSelecionados) throws Exception {
        JapeSession.SessionHandle hnd = null;

        try {
            hnd = JapeSession.open();
			
            final BoletoHelper helper = new BoletoHelper();
            final BoletoHelper.ConfiguracaoBoleto config = new BoletoHelper.ConfiguracaoBoleto();
            config.setTipoSaidaBoleto(1);
            config.setUsaContaBcoFinanceiros(true);
            config.setGerarNumeroBoleto(true);
            config.setReimprimirBoleta(true);
            config.setFinanceirosSelecionados(financeirosSelecionados);       	
            

            helper.gerarBoleto(config);
            return Base64.encodeBase64String(helper.getBoletosPDF());
        }
        finally {
            JapeSession.close(hnd);
        }
    }
}
