package br.com.sankhya.treinamento.utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class StringUtil {
	public static String trataTelefone(String telefone) {
		if (telefone == null) {
			return null;
		}

		String ret = retornaNumeros(telefone);

		if (ret.length() == 11) {
			ret = ret.substring(0, 1) + " " + ret.substring(2, 6) + " " + ret.substring(7, 10);
		} else if (ret.length() == 10) {
			ret = ret.substring(0, 1) + " " + ret.substring(2, 5) + " " + ret.substring(6, 9);
		} else {
			ret = null;
		}
		return ret;

	}

	public static String retornaNumeros(String str) {
		if (str == null) {
			return null;
		}
		StringBuffer ret = new StringBuffer();
		for (char rep : str.toCharArray()) {
			if (Character.isDigit(rep)) {
				ret = ret.append(rep);
			}
		}
		return ret.toString();

	}

	public static Timestamp toTimestamp(String str, String pattern) throws Exception {
		if(str.isEmpty())
			return null;
		
		
		Timestamp date = new Timestamp(new SimpleDateFormat(pattern).parse(str).getTime());
		return date;
	}

	public static String toString(Timestamp time, String pattern) throws Exception {
		if (time == null)
			return null;

		String date = new SimpleDateFormat(pattern).format(time);
		return date;
	}
}
