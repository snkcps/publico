package br.com.sankhya.treinamento.parsers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Element;

import br.com.sankhya.jape.EntityFacade;
import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.treinamento.utils.StringUtil;

public class PedidoParser {
	@SuppressWarnings("unchecked")
	public static DynamicVO parse(Element xml, DynamicVO pedidoVO) throws Exception {

		Element body = xml.getChild("Body", xml.getNamespace());
		Element notify = body.getChild("integrateOrderRequest", xml.getNamespace("boor"));
		Element order = notify.getChild("order", xml.getNamespace("ord"));

		String orderId = order.getChild("orderId", xml.getNamespace("ord")).getText();
		String totalAmount = order.getChild("totalAmount", xml.getNamespace("ord")).getText();
		String totalDiscountAmount = order.getChild("totalDiscountAmount", xml.getNamespace("ord")).getText();
		String purchaseDate = order.getChild("purchaseDate", xml.getNamespace("ord")).getText();
		String applicationVersion = order.getChild("applicationVersion", xml.getNamespace("ord")).getText();
		String saleChannel = order.getChild("saleChannel", xml.getNamespace("ord")).getText();

		List<Element> deliveryList = order.getChild("deliveries", xml.getNamespace("ord")).getChildren("delivery", xml.getNamespace("ord"));
		Element delivery = deliveryList.get(0);

		String deliveryId = delivery.getChild("deliveryId", xml.getNamespace("ord")).getText();
		//String deliveryOrderId = delivery.getChild("orderId", xml.getNamespace("ord")).getText();
		//String deliveryTotalAmount = delivery.getChild("totalAmount", xml.getNamespace("ord")).getText();
		//String deliveryTotalDiscountAmount = delivery.getChild("totalDiscountAmount", xml.getNamespace("ord")).getText();

		Element deliveryAddress = delivery.getChild("deliveryAddress", xml.getNamespace("ord"));
		String addressId = deliveryAddress.getChild("addressId", xml.getNamespace("ord")).getText();
		String recipientNm = deliveryAddress.getChild("recipientNm", xml.getNamespace("ord")).getText();
		String address = deliveryAddress.getChild("address", xml.getNamespace("ord")).getText();
		String addressNr = deliveryAddress.getChild("addressNr", xml.getNamespace("ord")).getText();
		
		String additionalInfo = null;
		
		try {
			additionalInfo = deliveryAddress.getChild("additionalInfo", xml.getNamespace("ord")).getText();
		} catch (Exception e) {
			additionalInfo = "";
		}
		
		
		String quarter = deliveryAddress.getChild("quarter", xml.getNamespace("ord")).getText();
		String city = deliveryAddress.getChild("city", xml.getNamespace("ord")).getText();
		String state = deliveryAddress.getChild("state", xml.getNamespace("ord")).getText();
		String countryId = deliveryAddress.getChild("countryId", xml.getNamespace("ord")).getText();
		String postalCd = deliveryAddress.getChild("postalCd", xml.getNamespace("ord")).getText();
		String friendlyNm = deliveryAddress.getChild("friendlyNm", xml.getNamespace("ord")).getText();
		String lift = deliveryAddress.getChild("lift", xml.getNamespace("ord")).getText();
		String intercom = deliveryAddress.getChild("intercom", xml.getNamespace("ord")).getText();

		Element customer = order.getChild("customer", xml.getNamespace("ord"));
		String customerId = customer.getChild("customerId", xml.getNamespace("ord")).getText();
		String documentNumber = customer.getChild("documentNumber", xml.getNamespace("ord")).getText();
		String name = customer.getChild("name", xml.getNamespace("ord")).getText();
		String email = customer.getChild("email", xml.getNamespace("ord")).getText();
		String phoneMobile = customer.getChild("phoneMobile", xml.getNamespace("ord")).getText();
		String phoneOffice = customer.getChild("phoneOffice", xml.getNamespace("ord")).getText();

		String state_subscription = null;

		try {
			state_subscription = customer.getChild("state_subscription", xml.getNamespace("ord")).getText();
		} catch (Exception e) {
			state_subscription = "";
		}

		String representativeNm = null;
		try {
			representativeNm = customer.getChild("representativeNm", xml.getNamespace("ord")).getText();

		} catch (Exception e) {
			representativeNm = "";
		}

		String createDt = customer.getChild("createDt", xml.getNamespace("ord")).getText();
		String updateDt = customer.getChild("updateDt", xml.getNamespace("ord")).getText();

		Element billingAddress = order.getChild("billingAddress", xml.getNamespace("ord"));
		String addressIdB = billingAddress.getChild("addressId", xml.getNamespace("ord")).getText();
		String recipientNmB = billingAddress.getChild("recipientNm", xml.getNamespace("ord")).getText();
		String addressB = billingAddress.getChild("address", xml.getNamespace("ord")).getText();
		String addressNrB = billingAddress.getChild("addressNr", xml.getNamespace("ord")).getText();
		
		String additionalInfoB = null;
		try {
			 additionalInfoB = billingAddress.getChild("additionalInfo", xml.getNamespace("ord")).getText();
			
		} catch (Exception e) {
			// TODO: handle exception
			additionalInfoB = "";
		}
		String quarterB = billingAddress.getChild("quarter", xml.getNamespace("ord")).getText();
		String cityB = billingAddress.getChild("city", xml.getNamespace("ord")).getText();
		String stateB = billingAddress.getChild("state", xml.getNamespace("ord")).getText();
		String countryIdB = billingAddress.getChild("countryId", xml.getNamespace("ord")).getText();
		String postalCdB = billingAddress.getChild("postalCd", xml.getNamespace("ord")).getText();
		String friendlyNmB = billingAddress.getChild("friendlyNm", xml.getNamespace("ord")).getText();
		String liftB = billingAddress.getChild("lift", xml.getNamespace("ord")).getText();
		String intercomB = billingAddress.getChild("intercom", xml.getNamespace("ord")).getText();
		List<Element> paymentList = (List<Element>) order.getChild("paymentList", xml.getNamespace("ord")).getChildren("payment", xml.getNamespace("ord"));

		Double couponValueNUmber = 0.0;
		String abstractType = "";
		String paymentType = "";
		String value = "0";
		String numberOfInstallments = "0";

		for (int i = 0; i < paymentList.size(); i++) {
			Element payment = paymentList.get(i);

			Element abstractPayment = payment.getChild("abstractPayment", xml.getNamespace("ord"));

			if (abstractPayment != null) {
				abstractType = abstractPayment.getChild("paymentType", xml.getNamespace("ord")).getText();

				if (abstractType.equals("COUPON")) {
					if (paymentType.equals(""))
						paymentType = "COUPON";

					couponValueNUmber += Double.valueOf(abstractPayment.getChild("value", xml.getNamespace("ord")).getText());

				} else {
					paymentType = abstractType;
					value = abstractPayment.getChild("value", xml.getNamespace("ord")).getText();
				}
			}

			Element creditCardPayment = payment.getChild("creditCardPayment", xml.getNamespace("ord"));

			if (creditCardPayment != null) {
				paymentType = creditCardPayment.getChild("paymentType", xml.getNamespace("ord")).getText();
				value = creditCardPayment.getChild("value", xml.getNamespace("ord")).getText();
				numberOfInstallments = creditCardPayment.getChild("numberOfInstallments", xml.getNamespace("ord")).getText();
			}
		}
		
		
		String couponValue = String.valueOf(couponValueNUmber);

		String freightChargedAmount = null;

		try {
			freightChargedAmount = order.getChild("freightChargedAmount", xml.getNamespace("ord")).getText();
		} catch (Exception e) {
			freightChargedAmount = "0";
		}

		String freightActualAmount = null;

		try {
			freightActualAmount = order.getChild("freightActualAmount", xml.getNamespace("ord")).getText();
		} catch (Exception e) {
			freightActualAmount = "0";
		}

		String countDistinctSku = order.getChild("countDistinctSku", xml.getNamespace("ord")).getText();

		//Order
		pedidoVO.setProperty("ORDERID", orderId);
		pedidoVO.setProperty("TOTALAMOUNT", new BigDecimal(totalAmount));
		pedidoVO.setProperty("TOTALDISCOUNTAMOUNT", new BigDecimal(totalDiscountAmount));
		pedidoVO.setProperty("PURCHASEDATE", StringUtil.toTimestamp(purchaseDate, "yyyy-MM-dd'T'HH:mm:ss"));
		pedidoVO.setProperty("APPLICATIONVERSION", applicationVersion);
		pedidoVO.setProperty("SALECHANNEL", saleChannel);
		pedidoVO.setProperty("FREIGHTCHARGEDAMOUNT", new BigDecimal(freightChargedAmount));
		pedidoVO.setProperty("FREIGHTACTUALAMOUNT", new BigDecimal(freightActualAmount));
		pedidoVO.setProperty("COUNTDISTINCTSKU", new BigDecimal(countDistinctSku));

		//Customer
		pedidoVO.setProperty("CUSTOMERID", customerId);
		pedidoVO.setProperty("DOCUMENTNUMBER", documentNumber);
		pedidoVO.setProperty("NAME", name);
		pedidoVO.setProperty("EMAIL", email);
		pedidoVO.setProperty("PHONEMOBILE", phoneMobile);
		pedidoVO.setProperty("PHONEOFFICE", phoneOffice);
		pedidoVO.setProperty("STATE_SUBSCRIPTION", state_subscription);
		pedidoVO.setProperty("REPRESENTATIVENM", representativeNm);
		pedidoVO.setProperty("CREATEDT", createDt);
		pedidoVO.setProperty("UPDATEDT", updateDt);

		// Billing Address

		pedidoVO.setProperty("ADDRESSID_B", addressIdB);
		pedidoVO.setProperty("RECIPIENTNM_B", recipientNmB);
		pedidoVO.setProperty("ADDRESS_B", addressB);
		pedidoVO.setProperty("ADDRESSNR_B", addressNrB);
		pedidoVO.setProperty("ADDITIONALINFO_B", additionalInfoB);
		pedidoVO.setProperty("QUARTER_B", quarterB);
		pedidoVO.setProperty("CITY_B", cityB);
		pedidoVO.setProperty("STATE_B", stateB);
		pedidoVO.setProperty("COUNTRYID_B", countryIdB);
		pedidoVO.setProperty("POSTALCD_B", postalCdB);
		pedidoVO.setProperty("FRIENDLYNM_B", friendlyNmB);
		pedidoVO.setProperty("LIFT_B", liftB);
		pedidoVO.setProperty("INTERCOM_B", intercomB);

		// Entrega

		pedidoVO.setProperty("DELIVERYID", deliveryId);

		pedidoVO.setProperty("ADDRESSID", addressId);
		pedidoVO.setProperty("RECIPIENTNM", recipientNm);
		pedidoVO.setProperty("ADDRESS", address);
		pedidoVO.setProperty("ADDRESSNR", addressNr);
		pedidoVO.setProperty("ADDITIONALINFO", additionalInfo);
		pedidoVO.setProperty("QUARTER", quarter);
		pedidoVO.setProperty("CITY", city);
		pedidoVO.setProperty("STATE", state);
		pedidoVO.setProperty("COUNTRYID", countryId);
		pedidoVO.setProperty("POSTALCD", postalCd);
		pedidoVO.setProperty("FRIENDLYNM", friendlyNm);
		pedidoVO.setProperty("LIFT", lift);
		pedidoVO.setProperty("INTERCOM", intercom);

		//Pagamento 

		pedidoVO.setProperty("COUPONVALUE", new BigDecimal(couponValue));
		pedidoVO.setProperty("PAYMENTTYPE", paymentType);
		pedidoVO.setProperty("VALUE", new BigDecimal(value));
		pedidoVO.setProperty("NUMBEROFINSTALLMENTS", new BigDecimal(numberOfInstallments));

		return pedidoVO;

	}

	@SuppressWarnings("unchecked")
	public static ArrayList<DynamicVO> parseItens(Element xml, EntityFacade dwf) throws Exception {

		Element body = xml.getChild("Body", xml.getNamespace());
		Element notify = body.getChild("integrateOrderRequest", xml.getNamespace("boor"));
		Element order = notify.getChild("order", xml.getNamespace("ord"));
		String orderId = order.getChild("orderId", xml.getNamespace("ord")).getText();

		List<Element> deliveryList = order.getChild("deliveries", xml.getNamespace("ord")).getChildren("delivery", xml.getNamespace("ord"));
		Element delivery = deliveryList.get(0);

		List<Element> orderLineList = delivery.getChild("orderLineList", xml.getNamespace("ord")).getChildren("orderLine", xml.getNamespace("ord"));

		ArrayList<DynamicVO> itens = new ArrayList<DynamicVO>();

		for (int ii = 0; ii < orderLineList.size(); ii++) {
			Element orderLine = orderLineList.get(ii);

			String sku = orderLine.getChild("sku", xml.getNamespace("ord")).getText();
			String skuType = orderLine.getChild("skuType", xml.getNamespace("ord")).getText();
			String quantity = orderLine.getChild("quantity", xml.getNamespace("ord")).getText();
			String catalogListPrice = orderLine.getChild("catalogListPrice", xml.getNamespace("ord")).getText();
			String listPrice = orderLine.getChild("listPrice", xml.getNamespace("ord")).getText();
			String salePrice = orderLine.getChild("salePrice", xml.getNamespace("ord")).getText();
			String unconditionalDiscountAmount = orderLine.getChild("unconditionalDiscountAmount", xml.getNamespace("ord")).getText();

			String conditionalDiscountAmount = null;
			try {
				conditionalDiscountAmount = orderLine.getChild("conditionalDiscountAmount", xml.getNamespace("ord")).getText();
			} catch (Exception e) {
				conditionalDiscountAmount = "0";
			}

			String roundingDiscountAmount = null;
			try {
				roundingDiscountAmount = orderLine.getChild("roundingDiscountAmount", xml.getNamespace("ord")).getText();
			} catch (Exception e) {
				roundingDiscountAmount = "0";
			}

			//String promotionList = orderLine.getChild("promotionList", xml.getNamespace("ord")).getText();

			String kitSkuId = null;
			try {
				kitSkuId = orderLine.getChild("kitSkuId", xml.getNamespace("ord")).getText();
			} catch (Exception e) {
				kitSkuId = "";
			}

			String kitSkuName = null;
			try {
				kitSkuName = orderLine.getChild("kitSkuName", xml.getNamespace("ord")).getText();
			} catch (Exception e) {
				kitSkuName = "";
			}

			String kitQuantity = null;
			try {
				kitQuantity = orderLine.getChild("kitQuantity", xml.getNamespace("ord")).getText();
			} catch (Exception e) {
				kitQuantity = "0";
			}

			String reference = null;
			try {
				reference = orderLine.getChild("reference", xml.getNamespace("ord")).getText();
			} catch (Exception e) {
				reference = "";
			}

			String service = null;
			try {
				service = orderLine.getChild("service", xml.getNamespace("ord")).getText();
			} catch (Exception e) {
				service = "N";
			}

			String skuName = orderLine.getChild("skuName", xml.getNamespace("ord")).getText();
			//String productBrandName = orderLine.getChild("productBrandName", xml.getNamespace("ord")).getText();

			//Item

			DynamicVO itemVO = (DynamicVO) dwf.getDefaultValueObjectInstance("InfracomItens");

			itemVO.setProperty("ORDERID", orderId);
			itemVO.setProperty("SKU", sku);
			itemVO.setProperty("SKUTYPE", skuType);
			itemVO.setProperty("QUANTITY", new BigDecimal(quantity));
			itemVO.setProperty("CATALOGLISTPRICE", new BigDecimal(catalogListPrice));
			itemVO.setProperty("LISTPRICE", new BigDecimal(listPrice));
			itemVO.setProperty("SALEPRICE", new BigDecimal(salePrice));
			itemVO.setProperty("UNCONDITIONALDISCOUNTAMOUNT", new BigDecimal(unconditionalDiscountAmount));
			itemVO.setProperty("CONDITIONALDISCOUNTAMOUNT", new BigDecimal(conditionalDiscountAmount));
			itemVO.setProperty("ROUNDINGDISCOUNTAMOUNT", new BigDecimal(roundingDiscountAmount));
			itemVO.setProperty("KITSKUID", kitSkuId);
			itemVO.setProperty("KITSKUNAME", kitSkuName);
			itemVO.setProperty("KITQUANTITY", new BigDecimal(kitQuantity));
			itemVO.setProperty("REFERENCE", reference);
			itemVO.setProperty("SERVICE", (service.equals("true") ? "S" : "N"));
			itemVO.setProperty("SKUNAME", skuName);

			Element freight = orderLine.getChild("freight", xml.getNamespace("ord"));

			if (freight != null) {
				String chargedAmount = freight.getChild("chargedAmount", xml.getNamespace("ord")).getText();
				String actualAmount = freight.getChild("actualAmount", xml.getNamespace("ord")).getText();
				
				String freightRoundingAmount = null;
				try {
					 freightRoundingAmount = freight.getChild("freightRoundingAmount", xml.getNamespace("ord")).getText();
					
				} catch (Exception e) {
					 freightRoundingAmount = "0";
				}
			
				String freightTime = freight.getChild("freightTime", xml.getNamespace("ord")).getText();
				String pickupLeadTime = freight.getChild("pickupLeadTime", xml.getNamespace("ord")).getText();
				String logisticContract = freight.getChild("logisticContract", xml.getNamespace("ord")).getText();

				String expectedDeliveryDate = null;

				try {
					expectedDeliveryDate = freight.getChild("expectedDeliveryDate", xml.getNamespace("ord")).getText();

				} catch (Exception e) {
					expectedDeliveryDate = "";
				}

				String adjustedDeliveryDate = null;

				try {
					adjustedDeliveryDate = freight.getChild("adjustedDeliveryDate", xml.getNamespace("ord")).getText();

				} catch (Exception e) {
					adjustedDeliveryDate = "";
				}

				
				itemVO.setProperty("CHARGEDAMOUNT", new BigDecimal(chargedAmount));
				itemVO.setProperty("ACTUALAMOUNT", new BigDecimal(actualAmount));
				itemVO.setProperty("COMMERCIALAMOUNT", new BigDecimal(freightRoundingAmount));
				itemVO.setProperty("FREIGHTTIME", new BigDecimal(freightTime));
				itemVO.setProperty("PICKUPLEADTIME", new BigDecimal(pickupLeadTime));
				itemVO.setProperty("LOGISTICCONTRACT", logisticContract);
				itemVO.setProperty("EXPECTEDDELIVERYDATE", StringUtil.toTimestamp(expectedDeliveryDate, "yyyy-MM-dd'T'HH:mm:ss"));
				itemVO.setProperty("ADJUSTEDDELIVERYDATE", StringUtil.toTimestamp(adjustedDeliveryDate, "yyyy-MM-dd'T'HH:mm:ss"));

			}
			itens.add(itemVO);
		}

		// TODO Auto-generated method stub
		return itens;
	}
}
