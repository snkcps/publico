package br.com.sankhya.treinamento.filters;
import javax.servlet.ServletException;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.FilterChain;
import javax.servlet.ServletResponse;
import javax.servlet.ServletRequest;
import br.com.sankhya.dwf.controller.servlet.HttpServiceBrokerFilter;

public class CorsFilter extends HttpServiceBrokerFilter
{
 public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain) throws IOException, ServletException {
     if (request instanceof HttpServletRequest) {
         ((HttpServletResponse)response).addHeader("Access-Control-Allow-Origin", ((HttpServletRequest)request).getHeader("Origin"));
         ((HttpServletResponse)response).addHeader("Access-Control-Allow-Credentials", "true");
         ((HttpServletResponse)response).addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD, PATCH");
         ((HttpServletResponse)response).addHeader("Access-Control-Max-Age", "1209600");
         ((HttpServletResponse)response).addHeader("Access-Control-Allow-Headers", "origin, content-type, accept, authorization, Access-Control-Request-Method, Access-Control-Request-Headers, X-ClbSession, token");
     }
     filterChain.doFilter(request, response);
 }
}
