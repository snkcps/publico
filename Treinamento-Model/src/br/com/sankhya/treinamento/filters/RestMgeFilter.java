// 
// Decompiled by Procyon v0.5.36
// 

package br.com.sankhya.treinamento.filters;

import javax.servlet.ServletException;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.FilterChain;
import javax.servlet.ServletResponse;
import javax.servlet.ServletRequest;
import br.com.sankhya.dwf.controller.servlet.DefaultMgeFilter;

public class RestMgeFilter extends DefaultMgeFilter
{
    public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse, final FilterChain filterChain) throws IOException, ServletException {
        if (((HttpServletRequest)servletRequest).getHeader("X-ClbSession") != null) {
            super.doFilter(servletRequest, servletResponse, filterChain);
        }
        else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }
}
