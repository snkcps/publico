package br.com.sankhya.modjava.listeners;

import br.com.sankhya.extensions.eventoprogramavel.EventoProgramavelJava;
import br.com.sankhya.jape.event.PersistenceEvent;
import br.com.sankhya.jape.event.TransactionContext;
import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.modjava.models.DadosCep;
import br.com.sankhya.modjava.services.BuscaCep;

public class CepListener implements EventoProgramavelJava {

	@Override
	public void afterDelete(PersistenceEvent arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterInsert(PersistenceEvent arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterUpdate(PersistenceEvent arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeCommit(TransactionContext arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeDelete(PersistenceEvent arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeInsert(PersistenceEvent event) throws Exception {
		// TODO Auto-generated method stub
		
		DynamicVO cepVO = (DynamicVO) event.getVo();
		
		DadosCep dados = BuscaCep.busca(cepVO.asString("CEP"));
		
		cepVO.setProperty("LOGRADOURO", dados.getLogradouro());
	}

	@Override
	public void beforeUpdate(PersistenceEvent event) throws Exception {
		// TODO Auto-generated method stub
		
		
		
		if(event.getModifingFields().isModifing("CEP")){
			DynamicVO cepVO = (DynamicVO) event.getVo();
			
			DadosCep dados = BuscaCep.busca(cepVO.asString("CEP"));
			
			cepVO.setProperty("LOGRADOURO", dados.getLogradouro());
			
			event.getModifingFields().setReavaliate(true);
		}
		
		
		
	}
	
}
