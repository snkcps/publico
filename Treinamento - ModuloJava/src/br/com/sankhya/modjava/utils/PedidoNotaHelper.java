package br.com.sankhya.modjava.utils;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.jdom.Element;

import br.com.sankhya.dwf.services.ServiceUtils;
import br.com.sankhya.jape.core.JapeSession;
import br.com.sankhya.jape.dao.EntityDAO;
import br.com.sankhya.jape.dao.EntityPropertyDescriptor;
import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.jape.wrapper.JapeFactory;
import br.com.sankhya.mgecomercial.model.centrais.cac.CACSP;
import br.com.sankhya.mgecomercial.model.centrais.cac.CACSPHome;
import br.com.sankhya.mgecomercial.model.facades.SelecaoDocumentoSP;
import br.com.sankhya.mgecomercial.model.facades.SelecaoDocumentoSPHome;
import br.com.sankhya.modelcore.MGEModelException;
import br.com.sankhya.modelcore.auth.AuthenticationInfo;
import br.com.sankhya.modelcore.comercial.centrais.CACHelper;
import br.com.sankhya.modelcore.util.DynamicEntityNames;
import br.com.sankhya.motazan.model.dao.EntityFacadeW;
import br.com.sankhya.motazan.model.dao.WrapperVO;
import br.com.sankhya.ws.ServiceContext;

import com.sankhya.util.XMLUtils;

public class PedidoNotaHelper {
	public boolean confimarNota(BigDecimal nuNota) throws Exception {
		EntityFacadeW dwf = EntityFacadeW.newDWF();

		WrapperVO notaVO = dwf.findByPK("CabecalhoNota", nuNota);

		if (!"L".equals(notaVO.asString("STATUSNOTA"))) {

			try {

				CACHelper cacHelper = new CACHelper();
				cacHelper.processarConfirmacao(nuNota);

				notaVO.reloadVO();

			} catch (Exception e) {

				notaVO.reloadVO();

				if (!"L".equals(notaVO.asString("STATUSNOTA"))) {
					throw e;
				}
			}

			if (!"L".equals(notaVO.asString("STATUSNOTA"))) {
				throw new MGEModelException("Falha na confirma��o. Nota n�o confirmada.");
			}
		}

		return "L".equals(notaVO.asString("STATUSNOTA"));
	}

	public BigDecimal incluirNota(final DynamicVO notaVO, final Collection<DynamicVO> itemColl) throws Exception {

		JapeSession.TXBlock txBlock = new JapeSession.TXBlock() {
			@SuppressWarnings("unchecked")
			@Override
			public void doWithTx() throws Exception {

				notaVO.setProperty("NUNOTA", null);
				notaVO.clearReferences();

				for (DynamicVO itemVO : itemColl) {
					itemVO.setProperty("NUNOTA", null);
					itemVO.setProperty("SEQUENCIA", null);
					itemVO.clearReferences();
				}

				EntityFacadeW dwf = EntityFacadeW.newDWF();

				EntityDAO notaDAO = dwf.getDAOInstance("CabecalhoNota");

				Element nota = new Element("nota");
				Element cabecalho = new Element("cabecalho");
				Element itens = new Element("itens");

				Map<String, EntityPropertyDescriptor> notaAllFields = notaDAO.getSQLProvider().getAllFieldsByName();

				for (String fieldName : notaAllFields.keySet()) {
					Element field = new Element(fieldName);
					field.addContent(notaDAO.getFieldAsString(notaVO.getProperty(fieldName), fieldName));
					cabecalho.addContent(field);
				}

				nota.addContent(cabecalho);

				ServiceContext currentContext = ServiceContext.getCurrent();
				try {
					ServiceContext ctx = new ServiceContext(null);
					ctx.setAutentication(AuthenticationInfo.getCurrent());
					//ctx.setTempFolder(AuthenticationInfo.getTempFolder());
					ctx.setRequestBody(new Element("requestBody"));
					ctx.makeCurrent();

					ctx.getRequestBody().addContent(nota);

					CACHelper cacHelper = new CACHelper();
					cacHelper.incluirAlterarCabecalhoNota(ctx);

					Element primaryKey = XMLUtils.getRequiredChild(ctx.getBodyElement(), "pk");
					for (Element field : (List<Element>) primaryKey.getChildren()) {
						if (notaVO.containsProperty(field.getName())) {
							notaVO.setProperty(field.getName(), notaDAO.getFieldTypedValue(XMLUtils.getContentAsString(field), field.getName()));
						}
					}

					notaDAO.populateDeattachedValueObject(notaVO, null, false, false, true, true);

					nota.removeContent();
					nota.addContent(itens);
					nota.setAttribute("NUNOTA", notaDAO.getFieldAsString(notaVO.getProperty("NUNOTA"), "NUNOTA"));

					EntityDAO itemDAO = dwf.getDAOInstance("ItemNota");
				
					Map<String, EntityPropertyDescriptor> itemAllFields = itemDAO.getSQLProvider().getAllFieldsByName();

					for (DynamicVO itemVO : itemColl) {

						itemVO.setProperty("NUNOTA", notaVO.getProperty("NUNOTA"));

						Element item = new Element("item");

						for (String fieldName : itemAllFields.keySet()) {
							Element field = new Element(fieldName);
							field.addContent(itemDAO.getFieldAsString(itemVO.getProperty(fieldName), fieldName));
							item.addContent(field);
						}

						itens.addContent(item);
					}

					ctx.getRequestBody().removeContent();
					ctx.getBodyElement().removeContent();

					ctx.getRequestBody().addContent(nota);
					
					try{
						cacHelper.incluirAlterarItemNota(ctx);
					}
					catch (Exception e) {
						JapeFactory.dao(DynamicEntityNames.CABECALHO_NOTA).delete(notaVO.asBigDecimal("NUNOTA")) ;
						throw e;
					}

				} finally {
					if(currentContext!= null){
						currentContext.makeCurrent();
					}
				}
			}
		};

		if (JapeSession.getCurrentSession().hasTransaction()) {
			txBlock.doWithTx();
		} else {
			JapeSession.getCurrentSession().getTopMostHandle().execWithTX(txBlock);
		}

		return notaVO.asBigDecimal("NUNOTA");
	}

	public void alterarItensNota(final BigDecimal nuNota, final Collection<DynamicVO> itemColl) throws Exception {

		JapeSession.TXBlock txBlock = new JapeSession.TXBlock() {
			@Override
			public void doWithTx() throws Exception {

				EntityFacadeW dwf = EntityFacadeW.newDWF();

				Element nota = new Element("nota");
				Element itens = new Element("itens");

				ServiceContext currentContext = ServiceContext.getCurrent();
				try {
					ServiceContext ctx = new ServiceContext(null);
					ctx.setAutentication(currentContext.getAutentication());
					ctx.setTempFolder(currentContext.getTempFolder());
					ctx.setRequestBody(new Element("requestBody"));
					ctx.makeCurrent();

					ctx.getRequestBody().addContent(nota);
					nota.addContent(itens);
					nota.setAttribute("NUNOTA", nuNota.toString());

					CACHelper cacHelper = new CACHelper();

					EntityDAO itemDAO = dwf.getDAOInstance("ItemNota");
					@SuppressWarnings("unchecked")
					Map<String, EntityPropertyDescriptor> itemAllFields = itemDAO.getSQLProvider().getAllFieldsByName();

					for (DynamicVO itemVO : itemColl) {

						Element item = new Element("item");

						for (String fieldName : itemAllFields.keySet()) {
							Element field = new Element(fieldName);
							field.addContent(itemDAO.getFieldAsString(itemVO.getProperty(fieldName), fieldName));
							item.addContent(field);
						}

						itens.addContent(item);
					}

					cacHelper.incluirAlterarItemNota(ctx);

				} finally {
					currentContext.makeCurrent();
				}
			}
		};

		if (JapeSession.getCurrentSession().hasTransaction()) {
			txBlock.doWithTx();
		} else {
			JapeSession.getCurrentSession().getTopMostHandle().execWithTX(txBlock);
		}

	}

	public void ligarVarItensIguais(final BigDecimal nuNotaOrig, final BigDecimal nuNota) throws Exception {

		JapeSession.TXBlock txBlock = new JapeSession.TXBlock() {
			@Override
			public void doWithTx() throws Exception {

				EntityFacadeW dwf = EntityFacadeW.newDWF();

				Collection<WrapperVO> itemOrigColl = dwf.findByFinder("ItemNota", "NUNOTA=?", nuNotaOrig);
				Collection<WrapperVO> itemColl = dwf.findByFinder("ItemNota", "NUNOTA=?", nuNota);

				CONTINUAR: for (WrapperVO itemVO : itemColl) {

					for (WrapperVO itemOrigVO : itemOrigColl) {

						if (itemVO.asBigDecimal("SEQUENCIA").compareTo(itemOrigVO.asBigDecimal("SEQUENCIA")) == 0) {

							DynamicVO ligacaoVO = dwf.newVO("CompraVendavariosPedido");

							ligacaoVO.setProperty("NUNOTA", itemVO.get("NUNOTA"));
							ligacaoVO.setProperty("SEQUENCIA", itemVO.get("SEQUENCIA"));
							ligacaoVO.setProperty("NUNOTAORIG", itemOrigVO.get("NUNOTA"));
							ligacaoVO.setProperty("SEQUENCIAORIG", itemOrigVO.get("SEQUENCIA"));

							ligacaoVO.setProperty("QTDATENDIDA", itemVO.get("QTDNEG"));

							dwf.createEntity("CompraVendavariosPedido", ligacaoVO);

							continue CONTINUAR;
						}
					}
				}

			}
		};

		if (JapeSession.getCurrentSession().hasTransaction()) {
			txBlock.doWithTx();
		} else {
			JapeSession.getCurrentSession().getTopMostHandle().execWithTX(txBlock);
		}
	}

	public void excluirNota(final BigDecimal nuNota) throws Exception {

		final CACHelper helper = new CACHelper();

		if (JapeSession.getCurrentSession().hasTransaction()) {
			EntityFacadeW.newDWF().removeByCriteria("CompraVendavariosPedido", "NUNOTA=? OR NUNOTAORIG = ?", new Object[] { nuNota, nuNota });
			helper.excluirNota(nuNota);
		} else {
			JapeSession.getCurrentSession().getTopMostHandle().execWithTX(new JapeSession.TXBlock() {
				@Override
				public void doWithTx() throws Exception {
					EntityFacadeW.newDWF().removeByCriteria("CompraVendavariosPedido", "NUNOTA=? OR NUNOTAORIG = ?", new Object[] { nuNota, nuNota });
					helper.excluirNota(nuNota);
				}
			});
		}
	}

	public void cancelarNota(final BigDecimal nuNota) throws Exception {
		final EntityFacadeW dwf = EntityFacadeW.newDWF();

		ServiceContext currentContext = ServiceContext.getCurrent();
		try {

			EntityDAO notaDAO = dwf.getDAOInstance("CabecalhoNota");
			DynamicVO notaVO = dwf.findByPK("CabecalhoNota", nuNota);
			String dtProt = notaDAO.getFieldAsString(notaVO.getProperty("DHPROTOC"), "DHPROTOC");

			ServiceContext ctx = new ServiceContext(null);
			ctx.setRequestBody(new Element("requestBody"));
			ctx.setAutentication(currentContext.getAutentication());
			ctx.setTempFolder(currentContext.getTempFolder());
			ctx.makeCurrent();

			Element parametros = new Element("parametros");
			Element notasCanceladas = new Element("notasCanceladas");
			Element nunota = new Element("nunota").addContent(String.valueOf(nuNota));
			Element notas = new Element("notas").setAttribute("dtProt", dtProt).setAttribute("nuNota", String.valueOf(nuNota));

			ctx.getRequestBody().addContent(parametros.addContent(notasCanceladas.addContent(nunota)));
			parametros.addContent(notas);

			SelecaoDocumentoSP selecaoDocumentoSP = (SelecaoDocumentoSP) ServiceUtils.getStatelessFacade(SelecaoDocumentoSPHome.JNDI_NAME, SelecaoDocumentoSPHome.class);
			CACSP cacSP = (CACSP) ServiceUtils.getStatelessFacade(CACSPHome.JNDI_NAME, CACSPHome.class);

			selecaoDocumentoSP.getInfoCancelamento(ctx);

			ctx.setRequestBody(new Element("requestBody"));
			ctx.getBodyElement().removeContent();

			notasCanceladas = (Element) notasCanceladas.clone();
			notasCanceladas.setAttribute("justificativa", "lan�amento incorreto");
			notasCanceladas.setAttribute("validarProcessosWmsEmAndamento", "false");
			ctx.getRequestBody().addContent(notasCanceladas);

			JapeSession.getCurrentSession().getTopMostHandle().execWithTX(new JapeSession.TXBlock() {
				@Override
				public void doWithTx() throws Exception {
					dwf.removeByCriteria("CompraVendavariosPedido", "NUNOTA=? OR NUNOTAORIG = ?", new Object[] { nuNota, nuNota });
				}
			});

			cacSP.cancelarNota(ctx);

		} finally {
			currentContext.makeCurrent();
		}
	}

	public Element buildNota(DynamicVO cabVO, Collection<DynamicVO> itemColl) throws Exception {
		Element nota = new Element("nota");
		Element cabecalho = buildCabecalho(cabVO);
		Element itens = buildItens(itemColl);

		if (cabVO.getProperty("NUNOTA") != null) {
			EntityFacadeW dwf = EntityFacadeW.newDWF();
			EntityDAO notaDAO = dwf.getDAOInstance("CabecalhoNota");
			nota.setAttribute("NUNOTA", notaDAO.getFieldAsString(cabVO.asBigDecimal("NUNOTA"), "NUNOTA"));
		}

		nota.addContent(cabecalho);
		nota.addContent(itens);

		return nota;
	}

	public Element buildCabecalho(DynamicVO cabVO) throws Exception {

		EntityFacadeW dwf = EntityFacadeW.newDWF();
		EntityDAO notaDAO = dwf.getDAOInstance("CabecalhoNota");

		Element cabecalho = new Element("cabecalho");

		@SuppressWarnings("unchecked")
		Map<String, EntityPropertyDescriptor> notaAllFields = notaDAO.getSQLProvider().getAllFieldsByName();

		for (String fieldName : notaAllFields.keySet()) {
			Element field = new Element(fieldName);
			field.addContent(notaDAO.getFieldAsString(cabVO.getProperty(fieldName), fieldName));
			cabecalho.addContent(field);
		}

		return cabecalho;
	}

	public Element buildItens(Collection<DynamicVO> itemColl) throws Exception {

		Element itens = new Element("itens");

		EntityFacadeW dwf = EntityFacadeW.newDWF();
		EntityDAO itemDAO = dwf.getDAOInstance("ItemNota");
		@SuppressWarnings("unchecked")
		Map<String, EntityPropertyDescriptor> itemAllFields = itemDAO.getSQLProvider().getAllFieldsByName();

		for (DynamicVO itemVO : itemColl) {

			Element item = new Element("item");

			for (String fieldName : itemAllFields.keySet()) {
				Element field = new Element(fieldName);
				field.addContent(itemDAO.getFieldAsString(itemVO.getProperty(fieldName), fieldName));
				item.addContent(field);
			}

			itens.addContent(item);
		}

		return itens;
	}
}
