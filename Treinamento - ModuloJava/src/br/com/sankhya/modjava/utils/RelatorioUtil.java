package br.com.sankhya.modjava.utils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import br.com.sankhya.jape.EntityFacade;
import br.com.sankhya.jape.dao.JdbcWrapper;
import br.com.sankhya.modelcore.MGEModelException;
import br.com.sankhya.modelcore.auth.AuthenticationInfo;
import br.com.sankhya.modelcore.comercial.util.print.converter.PrintConversionService;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;
import br.com.sankhya.modelcore.util.MGECoreParameter;
import br.com.sankhya.modelcore.util.Report;
import br.com.sankhya.modelcore.util.ReportManager;

import com.sankhya.util.StringUtils;

public class RelatorioUtil {

	public static byte[] gerarRelatorio(BigDecimal nuRfe, Map<String, Object> pk) throws Exception {

		EntityFacade dwf = EntityFacadeFactory.getDWFFacade();
		JdbcWrapper jdbc = dwf.getJdbcWrapper();

		try {
			jdbc.openSession();

			Map<String, Object> reportParams = buildReportParams(dwf, pk);

			Report report = ReportManager.getInstance().getReport(nuRfe, dwf);

			JasperPrint jasperPrint = report.buildJasperPrint(reportParams, jdbc.getConnection());

			byte[] conteudo = PrintConversionService.getInstance().convert(jasperPrint, byte[].class);

			return conteudo;

		} catch (Exception e) {
			Exception ee = new Exception("Erro relatorio nuRfe: " + nuRfe.toString());
			MGEModelException.throwMe(ee);
		} finally {
			jdbc.closeSession();
		}

		return null;

	}

	public static byte[] gerarRelatorioToPDF(BigDecimal nuRfe, Map<String, Object> pk) throws Exception {

		EntityFacade dwf = EntityFacadeFactory.getDWFFacade();
		JdbcWrapper jdbc = dwf.getJdbcWrapper();

		try {
			jdbc.openSession();

			Map<String, Object> reportParams = buildReportParams(dwf, pk);

			Report report = ReportManager.getInstance().getReport(nuRfe, dwf);

			JasperPrint jasperPrint = report.buildJasperPrint(reportParams, jdbc.getConnection());
			byte[] conteudo = JasperExportManager.exportReportToPdf(jasperPrint);

			return conteudo;

		} catch (Exception e) {
			Exception ee = new Exception("Erro relatorio nuRfe: " + nuRfe.toString());
			MGEModelException.throwMe(ee);
		} finally {
			jdbc.closeSession();
		}

		return null;

	}

	private static Map<String, Object> buildReportParams(EntityFacade dwf, Map<String, Object> pk) throws Exception {
		Map<String, Object> reportParams = new HashMap<String, Object>();

		String pastaModelos = StringUtils.getEmptyAsNull((String) MGECoreParameter.getParameter("os.diretorio.modelos"));

		reportParams.put("REPORT_CONNECTION", dwf.getJdbcWrapper().getConnection());
		reportParams.put("PDIR_MODELO", StringUtils.getEmptyAsNull(pastaModelos));
		reportParams.put("PCODUSULOGADO", AuthenticationInfo.getCurrent().getUserID());
		reportParams.put("PNOMEUSULOGADO", AuthenticationInfo.getCurrent().getName());

		for (Entry<String, Object> entry : pk.entrySet()) {
			reportParams.put(entry.getKey(), entry.getValue());
		}

		return reportParams;
	}
}
