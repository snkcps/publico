// 
// Decompiled by Procyon v0.5.36
// 

package br.com.sankhya.modjava.utils;

import java.math.BigDecimal;

import br.com.sankhya.jape.EntityFacade;
import br.com.sankhya.jape.dao.JdbcWrapper;
import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.jape.wrapper.JapeFactory;
import br.com.sankhya.jape.wrapper.JapeWrapper;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;

public class ConfigAPI {

	public BigDecimal	modPedido;
	public BigDecimal	codCfg;
	public String		usuario;
	public String		senha;
	public String		authWS;
	public String		epPrecoUri;
	public String		epEstoqueUri;
	public String		epAprovacaoClienteUri;
	public String		epRastreamentoPedidoUri;
	public String		epLimiteCreditoUri;
	public boolean		debugMode;
	public boolean		confirmaPedido;
	public BigDecimal	localPadrao;
	public String		epAutenticacao;
	public String		epPoliticaPreco;
	
	public ConfigAPI() throws Exception {
		this.codCfg = BigDecimal.valueOf(1);
		this.debugMode = true;

		JdbcWrapper jdbc = null;

		final EntityFacade dwfEntityFacade = EntityFacadeFactory.getDWFFacade();
		jdbc = dwfEntityFacade.getJdbcWrapper();
		jdbc.openSession();

		JapeWrapper configDAO = JapeFactory.dao("AD_IFCCFG");
		DynamicVO configVO = configDAO.findByPK(new Object[] { this.codCfg });
		this.usuario = configVO.asString("USUARIO");
		this.senha = configVO.asString("SENHA");
		this.authWS = "Basic " + Conversion.convertToBase64(String.valueOf(this.usuario) + ":" + this.senha);
		this.epPrecoUri = configVO.asString("EPPRECOURI");
		this.epEstoqueUri = configVO.asString("EPESTOQUEURI");
		this.epAprovacaoClienteUri = configVO.asString("EPAPROVCLIENTEURI");
		this.epRastreamentoPedidoUri = configVO.asString("EPRASTPEDIDOURI");
		this.epLimiteCreditoUri = configVO.asString("EPLIMITECREDITOURI");
		this.modPedido = configVO.asBigDecimal("MODPEDIDO");
		this.confirmaPedido = configVO.asBoolean("CONFPEDIDO");
		this.localPadrao = configVO.asBigDecimal("CODLOCAL");
		this.epAutenticacao = configVO.asString("EPAUTENTICACAO");
		this.epPoliticaPreco = configVO.asString("EPPOLITICAPRECO");
		
		jdbc.closeSession();
	}
}
