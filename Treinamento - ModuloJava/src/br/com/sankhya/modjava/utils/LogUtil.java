package br.com.sankhya.modjava.utils;

import br.com.sankhya.jape.EntityFacade;
import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.jape.vo.EntityVO;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;

public class LogUtil {
	public static String	SYNC;
	public static String	URL;
	public static String	REGISTRO;
	public static String	METHOD;
	public static String	REQUEST;
	public static String	RESPONSE;

	public static void saveLog(final Exception e) throws Exception {
		final StackTraceElement[] trace = e.getStackTrace();
		final String error = e.toString();
		final StringBuffer msg = new StringBuffer();
		StackTraceElement[] array;

		for (int length = (array = trace).length, i = 0; i < length; ++i) {
			final StackTraceElement erro = array[i];
			msg.append("\nFile:" + erro.getFileName() + " Class:" + erro.getClassName() + " Method: " + erro.getMethodName() + " Line:" + Integer.toString(erro.getLineNumber()));
			if (erro.getClassName().contains("snkcps")) {
				break;
			}
		}

		final String log = String.valueOf(e.getMessage()) + msg.toString();

		try {
			final EntityFacade dwfEntityFacade = EntityFacadeFactory.getDWFFacade();

			final DynamicVO logVO = (DynamicVO) dwfEntityFacade.getDefaultValueObjectInstance("AD_IFCLOG");
			logVO.setProperty("SYNC", LogUtil.SYNC);
			logVO.setProperty("URL", LogUtil.URL);
			logVO.setProperty("REGISTRO", LogUtil.REGISTRO);
			logVO.setProperty("METHOD", LogUtil.METHOD);
			logVO.setProperty("ERROR", error.substring(0, (error.length() > 100) ? 99 : (error.length() - 1)));
			if (log != null) {
				logVO.setProperty("LOG", log.toCharArray());
			}
			if (LogUtil.REQUEST != null) {
				logVO.setProperty("REQUEST", LogUtil.REQUEST.toCharArray());
			}
			if (LogUtil.RESPONSE != null) {
				logVO.setProperty("RESPONSE", LogUtil.RESPONSE.toCharArray());
			}
			dwfEntityFacade.createEntity("AD_IFCLOG", (EntityVO) logVO);
		} catch (Exception err) {
			throw err;

		}

	}
}
