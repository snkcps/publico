// 
// Decompiled by Procyon v0.5.36
// 

package br.com.sankhya.modjava.utils;

import com.thoughtworks.xstream.io.HierarchicalStreamDriver;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyReplacer;
import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;

public class Conversion
{
    public static String convertUTF8toISO(final String str) {
        String ret = null;
        try {
            ret = new String(str.getBytes("ISO-8859-1"), "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            return null;
        }
        return ret;
    }
    
    public static String convertToBase64(final String txt) throws Exception {
        final byte[] acessoByte = txt.getBytes("UTF-8");
        final String encoded = DatatypeConverter.printBase64Binary(acessoByte);
        return encoded;
    }
    
    public static String convertObjectToXML(final Object obj) throws Exception {
        final XStream stream = new XStream((HierarchicalStreamDriver)new DomDriver("UTF-8", new XmlFriendlyReplacer("__", "_")));
        stream.autodetectAnnotations(true);
        return stream.toXML(obj).replaceAll("\\u000a", "");
    }
}
