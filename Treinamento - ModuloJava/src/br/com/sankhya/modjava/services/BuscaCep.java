package br.com.sankhya.modjava.services;

import br.com.sankhya.modjava.models.DadosCep;

import com.google.gson.Gson;

public class BuscaCep {

	private static String	response;
	
	public static DadosCep busca(String cep) throws Exception {

		String url = "http://viacep.com.br/ws/:cep/json/";
		url = url.replace(":cep", cep);
		response = Api.get(url);


		DadosCep dados = new Gson().fromJson(response, DadosCep.class);

		return dados;
	}
	
/*
 * [
{
  "cep": "74482-080",
  "logradouro": "Estrada de São Pedro",
  "complemento": "",
  "bairro": "Parque Maracanã",
  "localidade": "Goiânia",
  "uf": "GO",
  "ibge": "5208707",
  "gia": "",
  "ddd": "62",
  "siafi": "9373"
},

{
  "cep": "74482-080",
  "logradouro": "Estrada de São Pedro",
  "complemento": "",
  "bairro": "Parque Maracanã",
  "localidade": "Goiânia",
  "uf": "GO",
  "ibge": "5208707",
  "gia": "",
  "ddd": "62",
  "siafi": "9373"
}
]

*/

}
