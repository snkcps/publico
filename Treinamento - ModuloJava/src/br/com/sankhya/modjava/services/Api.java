package br.com.sankhya.modjava.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import br.com.sankhya.modjava.utils.LogUtil;

public class Api {

	public static final int	maxResult	= 50;
	public static int		responseCode;

	public static String post(String url, String content) throws Exception {
		return doRequest(url, content, "POST");
	}

	public static String put(String url, String content) throws Exception {
		return doRequest(url, content, "PUT");
	}

	public static String patch(String url, String content) throws Exception {
		return doRequest(url, content, "PATCH");
	}

	private static String doRequest(String url, String content, String method) throws Exception {

		LogUtil.REQUEST = content;
		LogUtil.RESPONSE = null;

		LogUtil.METHOD = method;
		LogUtil.URL = url;

		URL obj = new URL(url);

		HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();

		postConnection.setConnectTimeout(5000);
		postConnection.setReadTimeout(600000);

		postConnection.setRequestMethod(method);

		postConnection.setRequestProperty("Content-Type", "application/json");
		//postConnection.setRequestProperty("Authorization", "Bearer " + accessToken);
		postConnection.setDoOutput(true);
		OutputStream os = postConnection.getOutputStream();
		os.write(content.getBytes());
		os.flush();
		os.close();
		responseCode = postConnection.getResponseCode();
		StringBuffer response = new StringBuffer();
		StringBuffer responseBody = new StringBuffer();
		response.append("\nPOST Response Code :  " + responseCode);
		response.append("\nPOST Response Message : " + postConnection.getResponseMessage());

		if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_CREATED || responseCode == HttpURLConnection.HTTP_ACCEPTED) { //success
			BufferedReader in = new BufferedReader(new InputStreamReader(postConnection.getInputStream()));
			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				responseBody.append("\n" + inputLine);
			}
			in.close();

		} else {

			if (postConnection.getErrorStream() != null) {
				BufferedReader in = new BufferedReader(new InputStreamReader(postConnection.getErrorStream()));
				String inputLine;

				while ((inputLine = in.readLine()) != null) {
					responseBody.append("\n" + inputLine);
				}
				in.close();

			}
			response.append("\nPOST NOT WORKED");

			throw new Exception(response.toString() + responseBody.toString());

		}

		LogUtil.RESPONSE = response.toString() + responseBody.toString();

		
		return responseBody.toString();
	}

	

	public static String get(String url) throws Exception {


		LogUtil.METHOD = "GET";
		LogUtil.URL = url;

		URL urlForGetRequest = new URL(url);
		//	String readLine = null;
		HttpURLConnection connection = (HttpURLConnection) urlForGetRequest.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Content-Type", "application/json");


		responseCode = connection.getResponseCode();
		StringBuffer response = new StringBuffer();
		StringBuffer responseBody = new StringBuffer();

		if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_CREATED || responseCode == HttpURLConnection.HTTP_ACCEPTED) { //success
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				responseBody.append("\n" + inputLine);
			}
			in.close();

		} else {

			if (connection.getErrorStream() != null) {
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
				String inputLine;

				while ((inputLine = in.readLine()) != null) {
					responseBody.append("\n" + inputLine);
				}
				in.close();
			}

			response.append("\nGET NOT WORKED");

			throw new Exception(response.toString() + responseBody.toString());
		}

		LogUtil.RESPONSE = response.toString() + responseBody.toString();


		return responseBody.toString();
	}

	public static String convertUTF8toISO(String str) {
		String ret = null;
		try {
			ret = new String(str.getBytes("ISO-8859-1"), "UTF-8");
		} catch (java.io.UnsupportedEncodingException e) {
			return null;
		}
		return ret;
	}

	public static String convertUnicode(String str) {
		StringBuffer ostr = new StringBuffer();

		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			/* caracter precisa ser convertido para unicode? */
			if ((ch >= 0x0020) && (ch <= 0x007e)) {
				/* n�o */
				ostr.append(ch);
			} else {
				/* sim */
				ostr.append("\\u"); /* formato de unicode padr�o */
				/* pega o valor hexadecimal do caracter */
				String hex = Integer.toHexString(str.charAt(i) & 0xFFFF);
				for (int j = 0; j < 4 - hex.length(); j++) {
					/* concatena o zero porque o unicode requer 4 digitos */
					ostr.append("0");
				}
				ostr.append(hex.toLowerCase());
			}
		}
		return (new String(ostr));
	}
	/*
	private static void allowMethods(String... methods) {
	    try {
	        Field methodsField = HttpURLConnection.class.getDeclaredField("methods");

	        Field modifiersField = Field.class.getDeclaredField("modifiers");
	        modifiersField.setAccessible(true);
	        modifiersField.setInt(methodsField, methodsField.getModifiers() & ~Modifier.FINAL);

	        methodsField.setAccessible(true);

	        String[] oldMethods = (String[]) methodsField.get(null);
	        Set<String> methodsSet = new LinkedHashSet<>(Arrays.asList(oldMethods));
	        methodsSet.addAll(Arrays.asList(methods));
	        String[] newMethods = methodsSet.toArray(new String[0]);

	        methodsField.set(null, newMethods);
	    } catch (NoSuchFieldException | IllegalAccessException e) {
	        throw new IllegalStateException(e);
	    }
	}	
	*/

}
