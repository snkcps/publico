package br.com.sankhya.modjava.actions;

import br.com.sankhya.extensions.actionbutton.AcaoRotinaJava;
import br.com.sankhya.extensions.actionbutton.ContextoAcao;
import br.com.sankhya.modjava.models.Atividade;
import br.com.sankhya.modjava.models.DadosCNPJ;
import br.com.sankhya.modjava.services.Api;

import com.google.gson.Gson;

public class BuscaReceitaBt implements AcaoRotinaJava {

	@Override
	public void doAction(ContextoAcao ctx) throws Exception {

		String cnpj = (String) ctx.getParam("CNPJ");

		String url = "https://www.receitaws.com.br/v1/cnpj/:cnpj";
		url = url.replace(":cnpj", cnpj);
		String response = Api.get(url);

		DadosCNPJ dados = new Gson().fromJson(response, DadosCNPJ.class);

		StringBuilder listAtiv = new StringBuilder();
		
		listAtiv.append("<br>");
		
		for (Atividade ativ : dados.atividade_principal){
			listAtiv.append(ativ.code + " - " + ativ.text + "<br>");
		}
		
		for (Atividade ativ : dados.atividades_secundarias){
			listAtiv.append(ativ.code + " - " + ativ.text + "<br>");
		}

			ctx.setMensagemRetorno("Empresa: " + dados.nome + listAtiv.toString());

	}
}
