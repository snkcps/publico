package br.com.sankhya.modjava.actions;

import br.com.sankhya.extensions.actionbutton.AcaoRotinaJava;
import br.com.sankhya.extensions.actionbutton.ContextoAcao;
import br.com.sankhya.extensions.actionbutton.Registro;
import br.com.sankhya.modjava.models.DadosCep;
import br.com.sankhya.modjava.services.BuscaCep;

public class BuscaCepBt implements AcaoRotinaJava {

	@Override
	public void doAction(ContextoAcao ctx) throws Exception {

			
			for (Registro linha : ctx.getLinhas()){
					
				    String cep = (String) linha.getCampo("CEP");
				    
					DadosCep dados = BuscaCep.busca(cep);
					
					linha.setCampo("LOGRADOURO", dados.getLogradouro());

					
					linha.save();
					
					ctx.setMensagemRetorno("CEP Buscado com Sucesso");
				
				
			}

		
	}
}
