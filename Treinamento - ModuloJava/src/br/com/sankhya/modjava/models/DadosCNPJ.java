package br.com.sankhya.modjava.models;

import java.util.ArrayList;

public class DadosCNPJ {

	public ArrayList<Atividade> atividade_principal;
	public String data_situacao;
	public String tipo;
	public String nome;
	public ArrayList<Atividade> atividades_secundarias;
	
	
}

/*{
"atividade_principal": [
{
    "text": "Com�rcio atacadista de ferragens e ferramentas",
    "code": "46.72-9-00"
}
],
"data_situacao": "25/02/2001",
"tipo": "MATRIZ",
"nome": "ANHANGUERA COMERCIO DE FERRAMENTAS LTDA",
"uf": "SP",
"telefone": "(19) 3116-4000",
"email": "fiscal@anhangueraferramentas.com.br",
"atividades_secundarias": [
{
    "text": "Manuten��o e repara��o de m�quinas-ferramenta",
    "code": "33.14-7-13"
},
{
    "text": "Representantes comerciais e agentes do com�rcio de madeira, material de constru��o e ferragens",
    "code": "46.13-3-00"
},
{
    "text": "Com�rcio varejista de ferragens e ferramentas",
    "code": "47.44-0-01"
},
{
    "text": "Aluguel de outras m�quinas e equipamentos comerciais e industriais n�o especificados anteriormente, sem operador",
    "code": "77.39-0-99"
},
{
    "text": "Educa��o profissional de n�vel t�cnico",
    "code": "85.41-4-00"
}
],
"qsa": [
{
    "qual": "49-S�cio-Administrador",
    "nome": "JOAO LUIZ COTRIN"
},
{
    "qual": "22-S�cio",
    "qual_rep_legal": "05-Administrador",
    "nome_rep_legal": "HUGO HENRIQUE DE ALMEIDA COTRIN",
    "nome": "G.A.H. PARTICIPACOES LTDA"
}
],
"situacao": "ATIVA",
"bairro": "POLO I ALTA TECNOLOGIA DE CAMPINAS",
"logradouro": "R RONALD CLADSTONE NEGRI",
"numero": "375",
"cep": "13.069-472",
"municipio": "CAMPINAS",
"porte": "DEMAIS",
"abertura": "18/04/1995",
"natureza_juridica": "206-2 - Sociedade Empres�ria Limitada",
"fantasia": "ANHANGUERA FERRAMENTAS",
"cnpj": "00.565.813/0001-29",
"ultima_atualizacao": "2021-03-18T17:03:39.604Z",
"status": "OK",
"complemento": "",
"efr": "",
"motivo_situacao": "",
"situacao_especial": "",
"data_situacao_especial": "",
"capital_social": "4072100.00",
"extra": {},
"billing": {
"free": true,
"database": true
}
}*/